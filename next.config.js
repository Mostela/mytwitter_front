/** @type {import('next').NextConfig} */
const { join } = require("path");

const nextConfig = {
  compress: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'd3qpgiukycq9t1.cloudfront.net',
        port: '',
        pathname: '/photos/**',
      },
      {
        protocol: 'https',
        hostname: '*.phncdn.com',
        port: '',
        pathname: '/**',
      },
      {
        protocol: 'https',
        hostname: '*.xvideos-cdn.com',
        port: '',
        pathname: '/**',
      },
      {
        protocol: 'https',
        hostname: '*.babepedia.com',
        port: '',
        pathname: '/**',
      },
      {
          protocol: 'https',
          hostname: '*.com',
          port: '',
          pathname: '/**',
      },
      {
          protocol: 'https',
          hostname: '*.com.br',
          port: '',
          pathname: '/**',
      }
    ],
  },
  sassOptions: {
    includePaths: [join(__dirname, "styles")],
  },
  crossOrigin: "use-credentials",
  env: {
    BACKEND_HOST: "https://d3qpgiukycq9t1.cloudfront.net/api",
    UNSPLASH_CLIENT_ID: '62pGbx81ZSHKKN4O-mBo33ofML4ZCj12FFcxTZo-goc'
  }
}

module.exports = nextConfig;
