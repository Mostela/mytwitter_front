interface ITagsModel {
    key: string
    name: string
    value: string
}

interface IModelOf {
    name: string,
    photo_profile: string
    tags: Array<ITagsModel>
}

export interface IOnlyfans {
    _id: string
    name: string
    url: string
    tags?: string
    description: string,
    provider: string,
    model: IModelOf
    thumbnail: string
    movieUrl?: string
    duration: string
    created_at: Date
    uploadDate: Date
}
