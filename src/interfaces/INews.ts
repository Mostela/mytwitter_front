export interface INews {
  id: string
  title: string
  url: string
  published: string,
  haveContent: boolean
  source: string
  content?: string
}
