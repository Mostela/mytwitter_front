export interface IPosts {
    _id: string
    nome: string
    visualizacao: number
    date_created: Date
    image: string
    date_last_view: Date
}
