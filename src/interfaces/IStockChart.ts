interface IStockChartContainer {
    legend?: string
}

interface IStockChart {
    label: string
    volume: number
    containers: Array<IStockChartContainer>
}

interface IStockHistory {
    date_close: string,
    value: number
}

interface IStockOperation {
    _id: string
    ticker_symbol: string
    historical_data: Array<IStockHistory>
    last_close_price: number
    fluctuation_percent: number
    fluctuation_status: string
    avg_price: number
}

interface IStockOperationAPI {
    _id: string,
    stock: IStockOperation
}

interface IJackpotInfo {
  _id: string
  jackpot_value: number
  fluctuation_percent: number
  fluctuation_status: string
}
