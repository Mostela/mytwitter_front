export enum TechType {
  LIBRARY= 'LIBRARY',
  FRAMEWORK = 'FRAMEWORK',
  CLOUD = 'CLOUD',
  UNKNOWN = 'UNKNOWN',
  DATABASE = 'DATABASE',
  SOFTWARE = 'SOFTWARE',
}

export interface ITechTags {
  key: string
  value: string
}

interface ITechPOC {
  _id: string
  poc: boolean
  result: boolean
  description: string
  caseUse: string
  dataDone: Date
}

export interface ITechTools {
  _id: string
  name: string
  description: string
  url: string
  type: TechType,
  poc: ITechPOC,
  usedRealLife: boolean,
  tags: Array<ITechTags>,
  date_created: Date
}

export interface ITechToolsList {
  count: number
  data: {
    techs: Array<ITechTools>,
    pocs: Array<ITechTools>
  }
}
