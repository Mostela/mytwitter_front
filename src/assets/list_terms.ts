export interface ITerms {
    author: string,
    word: string
    dynamicImage: string
}

export const List_terms: Array<ITerms> = [
    {
        "word": "Uma palavra grosseira, uma expressão bizarra, ensinou-me por vezes mais do que dez belas frases",
        "author": "Frued",
        "dynamicImage": "castels"
    },
    {
        "author": "Platao",
        "word": "Tente mover o mundo – o primeiro passo será mover a si mesmo.",
        "dynamicImage": "change the world"
    },
    {
        "word": "Quando vires um homem bom, tenta imitá-lo; quando vires um homem mau, examina-te a ti mesmo.",
        "author": "Confúcio",
        "dynamicImage": "leader"
    },
    {
        "word": "Escolhe um trabalho de que gostes e não terás que trabalhar nem um dia na tua vida.",
        "author": "Confúcio",
        "dynamicImage": "coding"
    },
    {
        "word": "Não ser descoberto numa mentira é o mesmo que dizer a verdade.",
        "author": "Aristoteles Onassis",
        "dynamicImage": "true"
    },
    {
        "author": "Pitágoras",
        "word": "Eduquem as crianças, para que não seja necessário punir os adultos.",
        "dynamicImage": "studing"
    },
    {
        "author": "Aristóteles",
        "word": "É fazendo que se aprende a fazer aquilo que se deve aprender a fazer.",
        "dynamicImage": "learning"
    },
    {
        "author": "Aristóteles",
        "word": "O ignorante afirma, o sábio duvida, o sensato reflete.",
        "dynamicImage": "penguins"
    },
    {
        "author": "Albert Einstein",
        "word": "Triste época! É mais fácil desintegrar um átomo do que um preconceito.",
        "dynamicImage": "lewis hammiton"
    },
    {
        "author": "Sheldon Cooper",
        "word": "Eu disse que podia entender, não que eu me importava.",
        "dynamicImage": "learning"
    },
    {
        "author": "Sheldon Cooper",
        "word": "Sim, isso quer dizer que você faz parte da desilusão cultural em massa de que a aparente posição do sol em relação as constelações na hora do seu nascimento afeta sua personalidade de alguma forma.",
        "dynamicImage": "signo"
    },
    {
        "author": "Sheldon Cooper",
        "word": "Eu não chuto. Como cientista eu chego a conclusões baseadas em observação e experimentação.",
        "dynamicImage": "atomo"
    },
    {
        "author": "Ganti",
        "word": "Somente vai no dialogo quem nao se garante no soco",
        "dynamicImage": "box fight"
    },
    {
        "author": "Pandora (God of War)",
        "word": "A esperança é o que nos fortalece! É por ela que estamos aqui! É por ela que lutamos quando todo o resto está perdido!",
        "dynamicImage": "pandora"
    },
    {
        "author": "Kratos",
        "word": "Tudo o que eu lembro é o que perdi.",
        "dynamicImage": "war"
    },
    {
        "author": "Batman",
        "word": "Faça seus medos terem medo de você.",
        "dynamicImage": "batman"
    },
    {
        "author": "Duas Caras - Batman",
        "word": "Ou você morre como herói, ou vive o bastante para se tornar o vilão",
        "dynamicImage": "dark knight"
    },
    {
        "author": "Tony Stark",
        "word": "Todos querem um dia feliz, certo? mas nem sempre ELE vem.",
        "dynamicImage": "stark"
    },
    {
        "author": "Arya Stark",
        "word": "Se deixar um lobo vivo, as ovelhas nunca estarão seguras.",
        "dynamicImage": "wolf black"
    },
    {
        "author": "Catelyn Stark",
        "word": "Um homem deve fazer as suas próprias escolhas.",
        "dynamicImage": "black"
    },
    {
        "author": "Thanos",
        "word": "Stark, você não é o único amaldiçoado com a sabedoria.",
        "dynamicImage": "universe"
    },
    {
        "author": "Clarice Lispector",
        "word": "Até cortar os próprios defeitos pode ser perigoso. Nunca se sabe qual é o defeito que sustenta nosso edifício inteiro.",
        "dynamicImage": "universe wallpaper"
    }
]
