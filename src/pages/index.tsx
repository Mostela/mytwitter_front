import {Col, Container, Row} from "react-bootstrap";
import Head from "next/head";
import React, {useContext, useEffect, useRef, useState} from "react";
import {FormLoginComponent} from "@/components/templates/FormLogin";
import {useRouter} from "next/router";
import {UserLoggedContext} from "@/state/context/UserLogged";
import {RandomImage} from "@/components/templates/RandomImage";
import {FormQuickpost} from "@/components/templates/FormQuickpost";
import {GetImageOfDay} from "@/state/service/GetImageOfDay";
import {INews} from "@/interfaces/INews";
import {GetCNNNews, GetNewsLatest} from "@/state/service/feed";
import {News} from "@/components/ui/News";
import {clearTicketName, StockCard} from "@/components/templates/StockInfo";
import {GetJackpot, GetStockListOperation} from "@/state/service/stocks";

export default function HomePage() {
    const router = useRouter();
    const [imageOfDay, setImageOfDay] = useState<string>("");
    const [newsLatest, setNewsLatest] = useState<Array<INews>>([]);
    const [jackpotInfo, setJackpotInfo] = useState<IJackpotInfo>();
    const [stockList, setStockList] = useState<Array<IStockOperationAPI>>([])
    const userLogged = useContext(UserLoggedContext);

    const loadingCNN = useRef(false);
    const ladingLatest = useRef(false);
    const loadingJackpotInfo = useRef(false);
    const loadingStock = useRef(false)

    const novoPost = () => {
        router.push("/quickpost");
    };

    const handlerStock = (ticket_id: string) => {
        router.push(`/stock/${clearTicketName(ticket_id)}`)
    }

    useEffect(() => {
        if (!imageOfDay) {
            GetImageOfDay().then(value => {
                const {url} = value;
                setImageOfDay(url);
            });
        }

        if (!loadingCNN.current) {
            GetCNNNews().then(value => {
                const {data} = value;
                setNewsLatest([...data.slice(0, 6), ...newsLatest]);
                loadingCNN.current = true
            })
        }

        if (!ladingLatest.current) {
            GetNewsLatest().then(value => {
                const {data} = value;
                setNewsLatest([...data.sort((x: { published: any; }) => x.published).slice(0, 6), ...newsLatest]);
                ladingLatest.current = true
            })
        }

        if (!loadingJackpotInfo.current) {
            GetJackpot().then(value => {
                setJackpotInfo(value)
                loadingJackpotInfo.current = true
            })
        }

        if (!loadingStock.current) {
            GetStockListOperation().then(value => {
                setStockList(value)
                loadingStock.current = true
            })
        }
    }, [imageOfDay, newsLatest, jackpotInfo]);

    return (
        <>
            <Head>
                <title>MyTwitter</title>
                <meta name="description" content="My Personal social network with all features that I think is cool!"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
            </Head>
            <main>
                {!userLogged.logged && <Container fluid>
                    <Row>
                        <Col
                            md={12}
                            className={"home-login"}>
                            <FormLoginComponent/>
                        </Col>
                    </Row>
                </Container>}

                {userLogged.logged && <Container className={"mt-3"} fluid={true}>
                    <Row>
                        <Col md={4} className={"p-2"}>
                            <Row>
                                <Col md={12} className={"p-2"}>
                                    <StockCard
                                legend={clearTicketName("Jackpot")}
                                selected={true}
                                onClick={() => handlerStock("")}
                                stockInfo={[{
                                    fluctuation: jackpotInfo?.fluctuation_percent.toPrecision(4).toString() + '%',
                                    lastPrice: jackpotInfo?.jackpot_value || 0
                                }, {
                                    fluctuation: jackpotInfo?.fluctuation_status.toLocaleUpperCase() || "",
                                    lastPrice: 0
                                }]}/>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={12} className={"p-2 row"}>
                                    {
                                        stockList.sort(x => x.stock.last_close_price).slice(0, 3).map((value) => {
                                            return <div className={"col-4"} key={value._id}>
                                                <StockCard
                                                    key={value.stock.ticker_symbol}
                                                    legend={clearTicketName(value.stock.ticker_symbol)}
                                                    onClick={() => handlerStock(value.stock.ticker_symbol)}
                                                    stockInfo={[{
                                                        fluctuation: value.stock.fluctuation_percent.toPrecision(4).toString() + '%',
                                                        lastPrice: value.stock.last_close_price
                                                    }, {
                                                        fluctuation: value.stock.fluctuation_status.toLocaleUpperCase(),
                                                        lastPrice: value.stock.avg_price
                                                    }]}/>
                                            </div>
                                        })
                                    }
                                </Col>
                            </Row>
                        </Col>
                        <Col md={8}>
                            <Row>
                                {
                                    newsLatest.map((value, index) => {
                                        return <Col key={index} className={"p-3"} md={3}>
                                            <News id={value.id}
                                                  key={value.id}
                                                  title={value.title}
                                                  url={value.url}
                                                  published={value.published}
                                                  haveContent={value.haveContent}
                                                  source={value.source}
                                                  buttonsEnable={true}
                                            /></Col>;
                                    })
                                }
                            </Row>
                        </Col>
                    </Row>
                    <Row>
                        <Col md={6} className={"p-2"}>
                            <RandomImage/>
                        </Col>
                        <Col md={6}>
                            <FormQuickpost onSubmit={novoPost}/>
                        </Col>
                    </Row>
                </Container>}
            </main>
        </>
    );
}
