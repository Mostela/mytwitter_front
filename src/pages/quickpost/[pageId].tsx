import Head from 'next/head'
import React from "react";
import { Container, Row } from "react-bootstrap";
import {Navbar} from "@/components/layout/Navbar";
import {PageQuickpost} from "@/components/templates/PageQuickpost";
import { ReturnQuickpost } from "@/state/service/ReturnQuickpost";
import { IQuickpost } from "@/interfaces/IQuickpost";
import { GetServerSideProps } from "next";


export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const {pageId} = query;
  const {data} = await ReturnQuickpost(Number(pageId))
  return {
    props: {
      list_posts: data,
      pageId,
    },
  }
}

export default function PagesQuickpost(props: {list_posts: Array<IQuickpost>, pageId: number}) {
  return (
    <>
      <Head>
        <title>Quickpost - Page {props.pageId}</title>
      </Head>
      <Container fluid={false}>
        <Row>
          <div className={"d-flex flex-column"}>
            <PageQuickpost key={'start0'} items={props.list_posts} />
          </div>
        </Row>
      </Container>
    </>
  )
}
