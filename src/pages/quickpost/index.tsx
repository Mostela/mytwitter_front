import Head from 'next/head'
import React, { useRef, useState } from "react";
import { Container, Row } from "react-bootstrap";
import { FormQuickpost } from "@/components/templates/FormQuickpost";
import {Navbar} from "@/components/layout/Navbar";
import {PageQuickpost} from "@/components/templates/PageQuickpost";
import InfiniteScroll from "react-infinite-scroll-component";
import {LoadingComponent} from "@/components/ui/Loading";
import {Progressbar} from "@/components/ui/Progressbar";
import { ReturnQuickpost, ReturnQuickpostServer } from "@/state/service/ReturnQuickpost";
import { IQuickpost } from "@/interfaces/IQuickpost";
import { ToUpAnchor } from "@/components/layout/ToUpAnchor";


export const getServerSideProps = async () => {
  const {data} = await ReturnQuickpostServer()
  return {
    props: {
      list_posts: data
    }
  }
}

export default function HomeQuickpost(props: {list_posts: Array<IQuickpost>}) {
    const [pageNumber, setPageNumber] = useState(10)
    const [pages, setPages] = useState([<PageQuickpost key={'start0'} items={props.list_posts} />])
    const [percent, setPercent] = useState(100);
    const originalURL = useRef<string>("")

    const loadMoreItems = () => {
      setPageNumber(pageNumber + 10)
      ReturnQuickpost(pageNumber).then(value => {
        const {data: list_post} = value
        return list_post
      }).then(value => {
        if(originalURL.current == ""){
          originalURL.current = window.location.protocol + "//" + window.location.host + window.location.pathname;
        }

        setPages([...pages, <PageQuickpost key={pageNumber} items={value} />])
        const newUrl = originalURL.current + '/' + pageNumber;
        window.history.pushState({ path: newUrl }, '', newUrl);
      })
      setPercent(100)
    }

    const newPost = (quickpost: IQuickpost) => {
      setPages([<PageQuickpost key={`created${pages.length}`} items={[quickpost]} />, ...pages ])
    }

    return (
        <>
            <Head>
                <title>Quickpost</title>
            </Head>
            <Progressbar value={percent} visible={true}/>
            <Container fluid={false}>
                <Row>
                  <div className={"d-flex flex-row justify-content-center my-2"}>
                    <FormQuickpost onSubmit={newPost}/>
                  </div>
                  <div className={"d-flex flex-column"}>
                    <InfiniteScroll
                      dataLength={pages.length}
                      hasMore={true}
                      loader={<LoadingComponent />}
                      className={"d-flex flex-column align-content-center justify-content-center"}
                      pullDownToRefreshThreshold={95}
                      next={loadMoreItems}
                      onScroll={() => {
                        setPercent(percent - 40)
                      }}
                    >
                      { pages }
                    </InfiniteScroll>
                    <ToUpAnchor active={pageNumber > 10} itemName={"navbar"}/>
                  </div>
                </Row>
            </Container>
        </>
    )
}
