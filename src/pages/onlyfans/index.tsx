import React, { useState, Suspense, useEffect, useRef } from "react";
import { Col, Container, Row } from "react-bootstrap";
import { FormOnlyfansComponent } from "@/components/templates/FormOnlyfans";
import { OnlyfansComponent } from "@/components/ui/Onlyfans";
import Head from "next/head";
import { ReturnOnlyfansServer } from "@/state/service/ReturnOnlyfansServer";
import { IOnlyfans } from "@/interfaces/IOnlyfans";
import { DeleteOnlyfans } from "@/state/service/DeleteOnlyfans";
import { NewOnlyfansService } from "@/state/service/NewOnlyfans";
import { message } from "antd/lib";
import { Switch } from 'antd/lib';
import { GetImageOfDay } from "@/state/service/GetImageOfDay";
import { ImageQuickpost } from "@/components/templates/ImageQuickpost";
import { LoadingComponent } from "@/components/ui/Loading";
import { GetLocalStorage, SetLocalStorage } from "@/state/provider/localstorage";
import {randomUUID} from "crypto";

export const getServerSideProps = async () => {
  const { url } = await GetImageOfDay();
  return {
    props: {
      imageOfDay: url
    }
  };
};


export default function PageOnlyfans(props: { imageOfDay: string }) {
  const size_items = 2;

  const [listOnly, setListOnly] = useState<Array<IOnlyfans>>([]);
  const [messageApi, contextHolder] = message.useMessage();
  const [seeContent, setSeeContent] = useState<boolean>(false)
  const loadingData = useRef(true);

  useEffect(() => {
    if(loadingData.current){
      const cacheKey = GetLocalStorage("cacheKey-of") || ""
      ReturnOnlyfansServer(cacheKey).then(response => {
        const {data, headers} = response
        setListOnly(data.reverse());
        loadingData.current = !loadingData.current
        SetLocalStorage("cacheKey-of", headers.get('Mytwitter-Cache'))
      })
    }
  }, [loadingData, listOnly]);

  const setDataNewItem = (value: IOnlyfans) => {
    NewOnlyfansService({
      model: value.model.name,
      link: value.url
    }).then(value => {
      if (value) {
        messageApi.open({
          type: "success",
          content: 'new video created'
        })
        const cacheKey = randomUUID()
        ReturnOnlyfansServer(cacheKey).then(listNew => {
          const { data } = listNew;
          setListOnly(data.reverse());
          setSeeContent(!seeContent);
        });
      }
    });
  };

  const deleteItem = (guid: string) => {
    DeleteOnlyfans(guid).then(value => {
      if (value) {
        messageApi.open({
          type: "success",
          content: 'video deleted with success'
        })
        ReturnOnlyfansServer(randomUUID()).then(listNew => {
          const { data } = listNew;
          setListOnly(data.reverse());
        });
      }
    });
  };

  return (
    <>
      <Head>
        <title>Onlyfans</title>
      </Head>
      <Container fluid className={"mt-3"}>
        {contextHolder}
        <Row>
          <Col sm={12} md={4}>
            <div className={"d-flex p-3 text-center justify-content-center info-text"}>
              <FormOnlyfansComponent setData={setDataNewItem} />
            </div>
          </Col>
          <Col sm={12} md={4}>
            <div className={"d-flex p-3 text-center justify-content-center info-text"}>
              <div className={"w-25"} hidden={!seeContent}>
                <Suspense fallback={<p>Loading feed...</p>}>
                  <ImageQuickpost image={props.imageOfDay} text={"Image of day"} />
                </Suspense>
              </div>
            </div>
          </Col>
          <Col sm={12} md={4}>
            <div className={"p-3 text-center justify-content-center info-text"}>
              <h2 className={"h2"}>Others options</h2>
              <p>Think in all others options before click in this contents</p>
              <p>each minute here is a minute that you lost studying for AWS test</p>
              <small>Remember, you lost some money to do nothing</small>
              <div>
                <Switch unCheckedChildren="Visualizar conteudo"
                        checkedChildren="Ocultar conteudo"
                        onChange={() => setSeeContent(!seeContent)}
                        checked={seeContent}
                        defaultChecked />
              </div>
            </div>
          </Col>
        </Row>
        <Row>
          <Suspense fallback={<LoadingComponent show={true}/>}>
            {
              listOnly.map((value, index) => {
                return <Col md={size_items} className={"p-3"} key={value._id}>
                  <OnlyfansComponent
                    key={index}
                    OFModel={value}
                    deleteFun={deleteItem}
                    hiddenContent={!seeContent}
                  />
                </Col>;
              })
            }
          </Suspense>
        </Row>
      </Container>
    </>
  );
}
