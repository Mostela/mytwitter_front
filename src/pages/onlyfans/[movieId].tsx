import React from "react";
import { Container } from "react-bootstrap";
import Head from "next/head";
import { callMovieEndpoint } from "@/state/service/GetMovieContent";
import { GetServerSideProps } from "next";
import { IOnlyfans } from "@/interfaces/IOnlyfans";
import { MovieInfos } from "@/components/ui/MovieInfos";

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { movieId } = query;
  const { player: { url }, movie } = await callMovieEndpoint(String(movieId));
  return {
    props: {
      url,
      movie
    }
  };
};

export default function MovieOnlyfans(props: { url: string, movie: IOnlyfans }) {

  return (
    <>
      <Head>
        <title>MyTwitter - {props.movie.name}</title>
      </Head>
      <Container fluid className={"mt-3"}>
        <section className={"d-flex flex-column align-content-center align-middle"}>
          <div>
            <MovieInfos data={props.movie} />
          </div>
        </section>
      </Container>
    </>
  );
}
