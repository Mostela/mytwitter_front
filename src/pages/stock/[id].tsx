import Head from "next/head";
import {Container} from "react-bootstrap";
import {useRef, useState} from "react";
import {GetServerSideProps} from "next";
import {GetStockOperation} from "@/state/service/stocks";
import StockBarChart from "@/components/templates/StockBarChart";
import {clearTicketName, StockCard} from "@/components/templates/StockInfo";


export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const { id } = query;
  return {
    props: {
      ticket_id: id,
    }
  };
};

export default function IdStockPage(props: { ticket_id: string }) {
    const [stockData, setStockData] = useState<Array<IStockChart>>([])
    const [stockInfos, setStockInfos] = useState<IStockOperationAPI>()
    const loadingStock = useRef(true)

    if (loadingStock.current) {
        GetStockOperation(props.ticket_id).then(value => {
            const [stock] = value
            setStockInfos(stock)

            let startValue = 0
            setStockData(stock?.stock.historical_data.map((value: IStockHistory) => {
                let sizeContainer = Number(value.value.toFixed(0))
                if (sizeContainer < 0) {
                    sizeContainer = 1
                }
                const legend = `R$: ${value.value.toPrecision(4)}`

                if(startValue === 0) {
                    startValue = value.value
                }
                const containerList = [{legend}, containerDiffPrice(startValue, value.value)]
                startValue = value.value
                return {
                    label: value.date_close, volume: sizeContainer,
                    containers: [...containerList, ...Array(sizeContainer - containerList.length)]
                }
            }) || [])
        }).catch(() => {
            loadingStock.current = true
        }).finally(() => {
            loadingStock.current = false
        })
    }

    const containerDiffPrice = (start: number, end: number) => {
        return {legend: `${(((end - start) / end) * 100).toPrecision(4)} %`}
    }

    return (
        <Container fluid className={"mt-3"}>
            <Head>
                <title>MyTwitter - Stock - {props.ticket_id}</title>
            </Head>
            <section className={"row"}>
                <div className={"col-md-2 col-lg-2 col-sm-6"}>
                    <StockCard
                        key={stockInfos?.stock.ticker_symbol}
                        legend={clearTicketName(stockInfos?.stock.ticker_symbol || "")}
                        onClick={() => {}}
                        selected={true}
                        stockInfo={[{
                            fluctuation: stockInfos?.stock.fluctuation_percent.toPrecision(4).toString() + '%',
                            lastPrice: stockInfos?.stock.last_close_price || 0
                        }, {
                            fluctuation: stockInfos?.stock.fluctuation_status.toLocaleUpperCase() || "",
                            lastPrice: stockInfos?.stock.avg_price || 0
                        }]}/>
                </div>
            </section>
            <section className={"row"}>
                <div className={"col-12"}>
                    <StockBarChart data={stockData}/>
                </div>
            </section>
        </Container>
    )
}
