import StockBarChart from "@/components/templates/StockBarChart";
import Head from "next/head";
import {Container} from "react-bootstrap";
import {clearTicketName, StockCard} from "@/components/templates/StockInfo";
import {GetStockListOperation, GetStockOperation} from "@/state/service/stocks";
import {useRef, useState} from "react";


const stock_observer_list = ["JBSS3", "ITUB4", "MLAS3", "TOTS3", "BBDC3", "AMAR3", "PETR3"]

export default function HomeStock() {
    const [stockList, setStockList] = useState<Array<IStockOperationAPI>>([])
    const [stockData, setStockData] = useState<Array<IStockChart>>([])
    const [stockObserverList, setStockObserverList] = useState<Array<IStockOperationAPI>>([])
    const loadingStock = useRef(true)
    const loadingStockObserver = useRef(true)

    const [visibleObserver, setVisibleObserver] = useState(false)

    if (loadingStock.current) {
        GetStockListOperation().then(value => {
            setStockList(value)
        }).finally(() => {
            loadingStock.current = false
        })
    }

    if (loadingStockObserver.current && !loadingStock.current){
        const stockObserverListData: Array<IStockOperationAPI> = []
        stock_observer_list.forEach(value => {
            GetStockOperation(value).then(value => {
                stockObserverListData.push(value[0])
            })
        })
        loadingStockObserver.current = false
        setStockObserverList(stockObserverListData)
    }

    const containerDiffPrice = (start: number, end: number) => {
        return {legend: `${(((end - start) / end) * 100).toPrecision(4)} %`}
    }

    const handleSelectCardClick = (ticket_id: string) => {
        let stockSelected = stockList.find(value => value.stock.ticker_symbol === ticket_id)
        if (!stockSelected){
            stockSelected = stockObserverList.find(value => value.stock.ticker_symbol === ticket_id)
        }
        if (stockSelected) {
            const {stock: {historical_data}} = stockSelected
            let startValue = 0
            setStockData(historical_data.map(value => {
                let sizeContainer = Number(value.value.toFixed(0))
                if (sizeContainer < 0) {
                    sizeContainer = 1
                }
                const legend = `R$: ${value.value.toPrecision(4)}`

                if(startValue === 0) {
                    startValue = value.value
                }
                const containerList = [{legend}, containerDiffPrice(startValue, value.value)]
                startValue = value.value
                return {
                    label: value.date_close, volume: sizeContainer,
                    containers: [...containerList, ...Array(sizeContainer - containerList.length)]
                }
            }))
        }
    }

    return (
        <Container fluid className={"mt-3"}>
            <Head>
                <title>MyTwitter - Stock Market</title>
            </Head>
            <section className={"row"}>
                {
                    stockList.map((value) => {
                        return <div className={"col-md-2 col-lg-2 col-sm-6"} key={value._id}>
                            <StockCard
                                key={value.stock.ticker_symbol}
                                legend={clearTicketName(value.stock.ticker_symbol)}
                                onClick={() => handleSelectCardClick(value.stock.ticker_symbol)}
                                stockInfo={[{
                                    fluctuation: value.stock.fluctuation_percent.toPrecision(4).toString() + '%',
                                    lastPrice: value.stock.last_close_price
                                }, {
                                    fluctuation: value.stock.fluctuation_status.toLocaleUpperCase(),
                                    lastPrice: value.stock.avg_price
                                }]}/>
                        </div>
                    })
                }
            </section>
            <section className={"row my-3"}>
                <button type={'button'} className={"btn btn-outline-danger"} onClick={() => setVisibleObserver(!visibleObserver)}>Hidden Observer Stocks</button>
                <div className={"row p-2"} hidden={visibleObserver}>
                    {
                    stockObserverList.map((value: IStockOperationAPI) => {
                        return <div className={"col-md-2 col-lg-2 col-sm-6"} key={value._id}>
                            <StockCard
                                key={value.stock.ticker_symbol}
                                legend={clearTicketName(value.stock.ticker_symbol)}
                                onClick={() => handleSelectCardClick(value.stock.ticker_symbol)}
                                stockInfo={[{
                                    fluctuation: value.stock.fluctuation_percent.toPrecision(4).toString() + '%',
                                    lastPrice: value.stock.last_close_price
                                }, {
                                    fluctuation: value.stock.fluctuation_status.toLocaleUpperCase(),
                                    lastPrice: value.stock.avg_price
                                }]}/>
                        </div>
                    })
                }
                </div>
            </section>
            <section className={"row"}>
                <div className={"col-12"}>
                    <StockBarChart data={stockData}/>
                </div>
            </section>
        </Container>
    )
}
