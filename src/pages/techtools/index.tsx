import Head from "next/head";
import { Container } from "@/components/layout/Container";
import { Row } from "@/components/layout/Row";
import { Columns } from "@/components/layout/Columns";
import { PageTechPOC } from "@/components/templates/PageTechPOC";
import { GetTechTools } from "@/state/service/techlist";
import { ITechTools, ITechToolsList, TechType } from "@/interfaces/ITechTools";
import React, { useState } from "react";
import { TechSelectContext } from "@/state/context/TechSelect";
import { BtnPostComponent } from "@/components/ui/BtnPost";
import { TextFieldComponent } from "@/components/ui/TextField";
import { TypeTech } from "@/components/ui/TypeTech";
import { FormNewTech } from "@/components/templates/FormNewTech";
import { NewTechTags } from "@/state/service/techlist";
import { message } from 'antd/lib';
import { TechToShow } from "@/components/ui/TechToShow";
import { TechTabs } from "@/components/templates/TechTabs";

export const getServerSideProps = async () => {
  const response = await GetTechTools({})
  return {
    props: {
      list_tech: response,
    },
  }
}

export default function HomeTechTools(props: {list_tech: ITechToolsList}){
  const [listTech, setListTech] = useState<Array<ITechTools>>(props.list_tech.data.techs)
  const [listPocs, setListPocs] = useState<Array<ITechTools>>(props.list_tech.data.pocs)
  const [listTechCount, setListTechCount] = useState<number>(props.list_tech.count)
  const [techSelect, setTechSelect] = useState<ITechTools>()
  const [techCreate] = useState<ITechTools>()

  const [searchField, setSearchField] = useState<string>("")
  const [messageApi, contextHolder] = message.useMessage();


  const clearSelect = () => {
    setTechSelect(undefined);
  }

  const changeSearchItems = (event: any) => {
    setSearchField(event.target.value)
    if(!event.target.value){
      setListTech(props.list_tech.data.techs)
      setListPocs(props.list_tech.data.pocs)
    }else{
      setListTech(listTech.filter(value => value.name.toLowerCase().includes(searchField.toLowerCase())))
      setListPocs(listPocs.filter(value => value.name.toLowerCase().includes(searchField.toLowerCase())))
    }
  }

  const changeTypeTech = (event: any) => {
    const valueSearch = event.target.value;
    GetTechTools({
      searchParams: {
        type: valueSearch,
      }
    }).then(value => {
      setListTech(value.data.techs);
      setListTechCount(value.count);
    })
  }

  const createNewTech = (newItem: ITechTools) => {
    setTechSelect(newItem)
    messageApi.open({
      type: "loading",
      content: `Send new tech ${newItem.name} to create`
    })
    NewTechTags(newItem).then(value => {
      if(value.status){
        messageApi.open({
          type: "success",
          content: `New tech ${newItem.name} created`
        })
        const data = listTech;
        data.push(value.data)
        setListTech(data)
      }
    })
  }

  const resetList = () => {
    setListTech(props.list_tech.data.techs)
    setListTechCount(props.list_tech.count)
    setSearchField("")
  }

  return (
    <>
      <Head>
        <title>Tech Tools</title>
      </Head>
      {contextHolder}
      <Container>
        <TechSelectContext.Provider value={{techSelect, setTechSelect}}>
          <Row>
            {contextHolder}
            <Columns size={12}>
              <div className={"d-flex flex-md-row flex-sm-column justify-content-center align-items-center techlist-filter"}>
                <div className={"mx-2"} hidden={!(!!techSelect)}>
                  <BtnPostComponent status={false} sending={clearSelect}
                                    text={"Create new item"}
                  />
                </div>
                <div className={"mx-5"}>
                  <TextFieldComponent label={"Search tech by name"}
                                      value={searchField}
                                      onChange={changeSearchItems} type={"text"}
                  />
                </div>
                <div className={"mx-5"}>
                  <TypeTech value={TechType.FRAMEWORK} onChange={changeTypeTech} />
                </div>
                <div className={"mx-5"}>
                  <button className={"btn btn-outline-primary"}
                          type={"reset"}
                          onClick={resetList}
                  >Reset list</button>
                </div>
              </div>
            </Columns>
          </Row>
          <div className={"row justify-content-center mt-5"}>
            <Columns size={3} className={"d-flex justify-content-center col-sm-12"}>
              <PageTechPOC data={listPocs} />
            </Columns>
            <Columns size={6} className={"d-flex justify-content-center"}>
              <h4 className={"techlist"}>LIST OF TECHS</h4>
              <span className={"techlist"}>Count {listTechCount}</span>
              <div className={"d-flex flex-column"}>
                {
                  listTech.map(value => {
                    return <TechToShow
                      key={value._id}
                      dataRaw={value}
                    />
                  })
                }
              </div>
            </Columns>
            <Columns size={2} className={"d-flex justify-content-center"} hidden={!!techSelect}>
              <div className={"mx-1 my-3 p-1"}>
                <h4 className={"h4 text-center techlist"}>Save your new tech for future use</h4>
                <FormNewTech onSubmit={createNewTech} newTech={techCreate}/>
              </div>
            </Columns>
            <Columns size={2} className={"d-flex justify-content-center"} hidden={!(!!techSelect)}>
              <TechTabs techSelected={techSelect} />
            </Columns>
          </div>
        </TechSelectContext.Provider>
      </Container>
    </>
  )
}
