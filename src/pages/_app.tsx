import '@/style/global.sass'
import type { AppProps } from 'next/app'
import React, { useEffect, useState } from "react";
import { UserLoggedContext } from "@/state/context/UserLogged";
import { GetLocalStorage } from "@/state/provider/localstorage";
import Layout from "@/components/layout/Layout";

export default function App({ Component, pageProps }: AppProps) {
  const [logged, setLogged] = useState<boolean>(false);

  const loggedUpdate = (value: boolean) => {
    setLogged(value)
  }

  useEffect(() => {
    if(GetLocalStorage("logged") == "true"){
      setLogged(true)
    }
  }, [])

  return (
    <UserLoggedContext.Provider value={{logged, setLogged: loggedUpdate}}>
      <Layout>
        <Component {...pageProps} />
      </Layout>
    </UserLoggedContext.Provider>
  )
}
