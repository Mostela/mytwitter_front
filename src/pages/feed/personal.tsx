import Head from "next/head";
import React, { useState } from "react";
import { Container } from "@/components/layout/Container";
import { GetNewsPersonal } from "@/state/service/feed";
import { INews } from "@/interfaces/INews";
import { Row } from "@/components/layout/Row";
import { RenderItems } from "@/components/templates/RenderNews";


export const getServerSideProps = async () => {
  const { data } = await GetNewsPersonal(1);
  return {
    props: {
      list_news: data
    }
  };
};

export default function FeedCNNPage(props: { list_news: Array<INews> }) {
  const [listNews] = useState<Array<INews>>(props.list_news);
  return (
    <>
      <Head>
        <title>MyTwitter - Saved News</title>
      </Head>
      <Container>
        <Row>
          <RenderItems data={listNews} key={1}/>
        </Row>
      </Container>
    </>
  );
}
