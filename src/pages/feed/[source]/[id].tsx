import Head from "next/head";
import React from "react";
import { GetCNNNewsSearch, GetNewsLatestID } from "@/state/service/feed";
import { INews } from "@/interfaces/INews";
import { Col, Container } from "react-bootstrap";
import { Row } from "@/components/layout/Row";
import { GetServerSideProps } from "next";
import { LoadingComponent } from "@/components/ui/Loading";
import Link from "next/link";
import { ToolBarNews } from "@/components/templates/ToolBarNews";
import {stockInfoLink} from "@/utils/stock_info";


export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const {id, source} = query;
  let newsSelected;
  if(source === "cnn"){
    const { data } = await GetCNNNewsSearch(String(id));
    newsSelected = data[0];
  }else{
    newsSelected = await GetNewsLatestID(String(id));
  }
  return {
    props: {
      newsSelected
    }
  };
};

export default function PageNewsId(props: { newsSelected: INews }) {
  const applyPoliticalName = (text: string) => {
    const listNames = ["Lula", "Bolsonaro", "Alckmin", "Janja",
      "Trump", "Tarcísio", "Alexandre de Moraes", "Musk"]
    let textResponse = text
    for (const name of listNames) {
      textResponse = textResponse.replaceAll(name, `<span class='political-name'>${name}</span>`)
    }
    return textResponse
  }

  const clearText = (text: string) => {
    let textResponse;
    textResponse = stockInfoLink(applyPoliticalName(text))
    return textResponse
  }

  return (
    <>
      <Head>
        <title>MyTwitter - {props.newsSelected?.title}</title>
      </Head>
      <Container fluid={false}>
        <LoadingComponent show={!!props.newsSelected} />
        <Row>
          <ToolBarNews  news={props.newsSelected} buttonsEnable={true}/>
          <Col md={12} className={"feed-page"} hidden={!(!!props.newsSelected)}>
            <h1>{props.newsSelected.title}</h1>
            <div dangerouslySetInnerHTML={{ __html: clearText(props.newsSelected.content || '')}} />
          </Col>
        </Row>
        <div>
          <div className={"btn btn-primary text-center"}>
            <Link href={props.newsSelected.url} target={"_parent"}>
              <p>Keep reading this on {props.newsSelected.source}</p>
            </Link>
          </div>
        </div>
      </Container>
    </>
  );
}
