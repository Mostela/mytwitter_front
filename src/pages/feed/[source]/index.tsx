import { GetNewsLatestSource } from "@/state/service/feed";
import { INews } from "@/interfaces/INews";
import Head from "next/head";
import { Navbar } from "@/components/layout/Navbar";
import { Container } from "@/components/layout/Container";
import { Row } from "@/components/layout/Row";
import { Col } from "react-bootstrap";
import { News } from "@/components/ui/News";
import React from "react";
import { GetServerSideProps } from "next";

export const getServerSideProps: GetServerSideProps = async ({ query }) => {
  const {source} = query;
  const { data: newsSelected } = await GetNewsLatestSource(String(source), 0);
  return {
    props: {
      listNews: newsSelected,
      sourceName: source
    }
  };
};

export default function FeedSourcePage (props: { listNews: Array<INews>, sourceName: string }) {
  return (
    <>
      <Head>
        <title>MyTwitter - Feed {props.sourceName}</title>
      </Head>
      <Navbar />
      <Container>
        <Row>
          {
            props.listNews.map((value, index) => {
              return <Col key={index} className={"p-3"} md={2}>
                <News id={value.id}
                      key={value.id}
                      title={value.title}
                      url={value.url}
                      published={value.published}
                      haveContent={value.haveContent}
                      buttonsEnable={true}
                      source={value.source} /></Col>;
            })
          }
        </Row>
      </Container>
    </>
  )
}
