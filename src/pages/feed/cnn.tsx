import { News } from "@/components/ui/News";
import Head from "next/head";
import { Navbar } from "@/components/layout/Navbar";
import React, { useEffect, useRef, useState } from "react";
import { Container } from "@/components/layout/Container";
import { GetCNNNews } from "@/state/service/feed";
import { INews } from "@/interfaces/INews";
import { Col } from "react-bootstrap";
import { Row } from "@/components/layout/Row";
import { LoadingComponent } from "@/components/ui/Loading";

export default function FeedCNNPage() {
  const loadingData = useRef(true);
  const [cnnData, setCnnData] = useState<Array<INews>>([]);

  useEffect(() => {
    if(loadingData.current){
      GetCNNNews().then(value => {
        setCnnData(value.data)
      }).finally(
        () => {
          loadingData.current = !loadingData.current
        }
      )
    }
  }, []);

  return (
    <>
      <Head>
        <title>MyTwitter - Last News</title>
      </Head>
      <Container>
        <Row>
          <LoadingComponent show={!loadingData.current} loadingText={"Loading CNN News"} />
          {
            cnnData.map((value, index) => {
              return <Col key={index} className={"p-3"}  md={2}>
                <News id={value.id}
                      key={value.id}
                      title={value.title}
                      url={value.url}
                      published={value.published}
                      haveContent={value.haveContent}
                      source={value.source}
                      buttonsEnable={false}
                /></Col>;
            })
          }
        </Row>
      </Container>
    </>
  );
}
