import Head from "next/head";
import React, {useState} from "react";
import {Container} from "@/components/layout/Container";
import {Row} from "@/components/layout/Row";
import {INews} from "@/interfaces/INews";
import {GetNewsLatest} from "@/state/service/feed";
import {LoadingComponent} from "@/components/ui/Loading";
import {GetServerSideProps} from "next";
import {RenderItems} from "@/components/templates/RenderNews";
import InfiniteScroll from "react-infinite-scroll-component";

export const getServerSideProps: GetServerSideProps = async () => {
    const {data} = await GetNewsLatest(1)
    return {
        props: {
            lastNews: data
        }
    }
}

export default function Feed(props: { lastNews: Array<INews> }) {
    const [lastsNews] = useState<Array<INews>>(props.lastNews);
    const [pageNumber, setPageNumber] = useState(1)
    const [pages, setPages] = useState([
        <RenderItems data={lastsNews} key={pageNumber}/>
    ])

    const loadMoreItems = () => {
        setPageNumber(pageNumber + 1)
        GetNewsLatest(pageNumber).then(value => {
            const {data: list_post} = value
            return list_post
        }).then(value => {
            setPages([...pages, <RenderItems data={value} key={pageNumber}/>])
        })
    }

    return (
        <>
            <Head>
                <title>MyTwitter - Feed</title>
            </Head>
            <Container>
                <Row>
                    <InfiniteScroll
                        dataLength={pages.length}
                        hasMore={true}
                        loader={<LoadingComponent/>}
                        pullDownToRefreshThreshold={95}
                        next={loadMoreItems}
                    >
                        {pages}
                    </InfiniteScroll>
                </Row>
            </Container>
        </>
    )
}
