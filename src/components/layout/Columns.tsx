import React from 'react';
import classNames from 'classnames';

export interface ColumnsProps {
    className?: string
    size?: number
    center?: boolean
    children?: React.ReactNode
    hidden?: boolean
}

export const Columns = (props: ColumnsProps) => {
    const classCSS = classNames(props.size ? `col-md-${props.size} ` : 'col-md ' + props.className, {
        'text-center': props.center,
        'col-sm-6': true
    })

    return <div className={classCSS} hidden={props.hidden}>
        {props.children}
    </div>
}
