import React, { useContext, useState } from "react";
import { Col, Container, Image, Nav, Row } from "react-bootstrap";
import Link from "next/link";
import { selectIcon } from "@/assets/list_icons";
import { UserLoggedContext } from "@/state/context/UserLogged";
import { ButtonLogoff } from "@/components/templates/ButtonLogoff";
import { ICONS_PAGES, LinkNavComponent } from "@/components/ui/LinkNavComponent";

interface INavbar {}

export const Navbar: React.FC<INavbar> = () => {
    const userLogged = useContext(UserLoggedContext)
    const [image_logo, ] = useState<string>(selectIcon())

    return (
        <nav className={'nav navbar-custom'} id={"navbar"}>
            <Container fluid={'xl'}>
                <Row>
                    <Col>
                        <Link href={"/"}>
                            <Image src={image_logo} width={75} alt={"Logo MyTwiiter"}/>
                        </Link>
                    </Col>
                    <Col>
                        <Nav className="mt-2 justify-content-center">
                            <div>
                                <div hidden={!userLogged.logged} className={'btn-group'}>
                                    <Nav.Item>
                                        <LinkNavComponent link={'/'} icon={ICONS_PAGES.HOME}/>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <LinkNavComponent link={'quickpost'} icon={ICONS_PAGES.QUICK_POST}/>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <LinkNavComponent link={'onlyfans'} icon={ICONS_PAGES.HOT_LINK}/>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <LinkNavComponent link={'techtools'} icon={ICONS_PAGES.TECH}/>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <LinkNavComponent link={'feed/personal'} icon={ICONS_PAGES.NEWS_PERSONAL}/>
                                    </Nav.Item>
                                </div>
                                <div className={'btn-group'}>
                                    <Nav.Item>
                                        <LinkNavComponent link={'feed'} icon={ICONS_PAGES.NEWS_TECH}/>
                                    </Nav.Item>
                                    <Nav.Item>
                                        <LinkNavComponent link={'feed/cnn'} icon={ICONS_PAGES.NEWS}/>
                                    </Nav.Item>
                                    <Nav.Item hidden={!userLogged.logged}>
                                        <ButtonLogoff />
                                    </Nav.Item>
                                </div>
                            </div>
                        </Nav>
                    </Col>
                </Row>
            </Container>
        </nav>
    )
}
