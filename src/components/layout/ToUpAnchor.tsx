import React from "react";
import { FaArrowCircleUp } from "react-icons/fa";

interface ToUpAnchor {
  itemName: string
  active: boolean
}

export const ToUpAnchor: React.FC<ToUpAnchor> = (props) => {

  return <a href={`#${props.itemName}`}
            hidden={!props.active}
            className={"to-up-anchor"}>
    <FaArrowCircleUp
      size={25}
    />
  </a>
}
