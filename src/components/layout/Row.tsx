import React from 'react';

export interface RowProps {
    children?: React.ReactNode
}

export const Row = (props: RowProps) => <div className={'row align-items-start d-flex'}>{props.children}</div>
