import React from 'react';


export interface ContainerProps {
    children?: React.ReactNode
}

export const Container = (props: ContainerProps) => (
        <div className={'container-fluid'}>{props.children}</div>
    )
