import React from "react";

interface TagModelInterface {
  name: string
  value: string
}


export const TagModel: React.FC<TagModelInterface> = (props ) => {
  let visible = true
  const getRandomColor = () => {
    const vibrantColors = [
      "#FF5252", // Vermelho
      "#FF9800", // Laranja
      "#FFEB3B", // Amarelo
      "#4CAF50", // Verde
      "#00BCD4", // Ciano
      "#2196F3", // Azul
      "#9C27B0", // Roxo
      "#F44336", // Vermelho escuro
      "#FFC107", // Amarelo escuro
      "#8BC34A"  // Verde claro
    ];

    const randomIndex = Math.floor(Math.random() * vibrantColors.length);
    return vibrantColors[randomIndex];
  }

  if(props.value == "None"){
    visible = false
  }

  return (
    <div className={"align-items-center m-1 tagmodel"} style={{
      backgroundColor: getRandomColor()
    }} hidden={!visible}>
      <span className={"text-center tagmodel-title"} style={{
        backgroundColor: getRandomColor()
      }}>{props.name}</span>
      <span className={"text-center tagmodel-value"}>{props.value}</span>
    </div>
  )
}
