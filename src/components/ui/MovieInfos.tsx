import React from "react";
import { IOnlyfans } from "@/interfaces/IOnlyfans";
import { TagModel } from "@/components/ui/TagModel";
import Link from "next/link";


interface MovieInfos {
  data: IOnlyfans
}
export const MovieInfos: React.FC<MovieInfos> = (props) => {
  return (
    <div className={"d-flex flex-column justify-content-around"}>
      <div>
        <h1 className={"h1 text-primary text-capitalize"}>{props.data.name}</h1>
        <Link href={props.data.url} className={"btn btn-outline-primary"}>
          Movie Original
        </Link>
      </div>

      <div>
        <div className={"card"}>
          <div className={"card-body"}>
            {/* eslint-disable-next-line @next/next/no-img-element */}
            <img src={props.data.model.photo_profile} alt={props.data.model.name}
                 className={"img-fluid card-img"}
            />
            <h2 className={"h2 text-capitalize text-center text-secondary"}>{props.data.model.name}</h2>
          </div>
        </div>
      </div>
      <div className={"d-flex flex-row m-5"}>
        <div className={"w-3"}>
          {props.data.model.tags.map((value) => {
            return <TagModel name={value.name} value={value.value} key={value.key}/>
          })}
        </div>
      </div>
    </div>
  )
}
