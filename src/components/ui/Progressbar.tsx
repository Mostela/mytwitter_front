import React, { useState } from "react";
import classNames from 'classnames';

interface BarProgressInterface {
    value: number
    visible: boolean
}
export const Progressbar: React.FC<BarProgressInterface> = (props) => {
    const [percent, setPercent] = useState<number>(props.value)

    const classCSS = classNames('progressbar', {
        'width': `${percent}%`
    })

    return (
        <div className={classCSS}/>)
}
