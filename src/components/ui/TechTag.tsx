import React from "react";

interface ITechTag {
  name: string
}

export const TechTag: React.FC<ITechTag> = (props) => {
  return (
    <span className={"tag-tech"}>{props.name}</span>
  )
}
