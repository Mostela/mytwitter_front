import React from "react";
import { FaWrench } from "react-icons/fa";

interface ITechList {
  name: string
  type: string
}

export const TechItemList: React.FC<ITechList> = (props) => {
  return (
    <div className={"m-2 p-1 tech-item"}>
      <div className={"d-flex flex-row align-items-center justify-content-center tech-item-list"}>
        <div className={"d-flex justify-content-center"}>
          <FaWrench className={"mx-2"}/>
        </div>
        <div className={"d-flex flex-column mx-2 tech-item-list-text"}>
          <span>{props.name}</span>
          <small>{props.type}</small>
        </div>
      </div>
    </div>
  )
}
