import React from 'react';
import {StockContainer} from "@/components/ui/StockContainer";

interface StockBarContainerProps {
    containers: Array<any>
    title: string
}

export const StockBarContainers: React.FC<StockBarContainerProps> = (props) => {
    return (
        <div className="stock-bar">
            <span className="stock-label-title">{props.title}</span>
            <div className="stock-bar-container">
                {props.containers.map((elem, i) => (
                    <StockContainer key={i} legend={elem?.legend}/>
                ))}
            </div>
        </div>
    );
};
