import React, { useContext } from "react";
import Link from "next/link";
import { TechTag } from "@/components/ui/TechTag";
import { ITechTools } from "@/interfaces/ITechTools";
import { TechSelectContext } from "@/state/context/TechSelect";
import { ButtonDelete } from "@/components/templates/ButtonDelete";
import { DeleteTechTools } from "@/state/service/techlist";

interface ITechToShow {
  dataRaw: ITechTools
}

export const TechToShow: React.FC<ITechToShow> = (props) => {
  const contextSelect = useContext(TechSelectContext);

  const changeContext = () => {
    contextSelect.setTechSelect(props.dataRaw)
  }

  const deleteItem = (event: React.MouseEvent<HTMLFormElement>, id: string) => {
    event.preventDefault();
    contextSelect.setTechSelect(undefined);
    DeleteTechTools(id).then();
  }

  return (
    <div className="card mx-2 my-1 p-1">
      <h5 className="card-header text-capitalize">{props.dataRaw.name}</h5>
      <div className="card-body">
        <p className="card-text">{props.dataRaw.description}</p>
        <p className="card-subtitle mb-2 text-muted">
          <small className={"text-capitalize"}>{props.dataRaw.type}</small>
        </p>
        <div className={"mb-3"}>
          {
            props.dataRaw.tags.map(value => {
              return <TechTag name={value.value} key={value.key}/>
            })
          }
        </div>
        <div className={"btn-group"}>
          <Link className={"btn btn-primary text-capitalize"} href={props.dataRaw.url} target={"_blank"}>{props.dataRaw.name}</Link>
          <button
            className={"btn btn-secondary text-capitalize"}
            onClick={() => changeContext()}
          >
            Edit
          </button>
          <ButtonDelete id={props.dataRaw._id} delete={deleteItem}
                        className={"btn btn-primary text-capitalize"}
          />
        </div>
      </div>
    </div>
  )
}
