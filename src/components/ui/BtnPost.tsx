import React from "react";
import {FaPencilAlt} from "react-icons/fa";

interface BtnPostInterface {
    sending: any,
    status: boolean
    text?: string
    children?: React.ReactNode
}

export const BtnPostComponent: React.FC<BtnPostInterface> = (props) => {
    const textBtn = props.text || "Post"

    const sending = (e: React.MouseEvent<HTMLButtonElement>) => {
        props.sending(e)
    }

    return (
        <button onClick={sending} hidden={props.status}
                className={'btn btn-post'}
                type={'submit'}>
            <FaPencilAlt /> {textBtn} {props.children}
        </button>
    )
}
