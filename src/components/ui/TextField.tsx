import React, { ChangeEventHandler } from "react";
import TextField from "@material-ui/core/TextField";

interface TextFieldInterface {
  label: string
  type?: string
  value: string
  onChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
  onBlur?: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
}

export const TextFieldComponent = (props: TextFieldInterface) => {
  return <TextField label={props.label}
                    type={props.type}
                    value={props.value}
                    onChange={props.onChange}
                    onBlur={props.onBlur}
  />
}
