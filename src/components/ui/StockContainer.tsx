import React from 'react';

interface StockContainerProps {
    opacity?: number
    legend?: string
}

export const StockContainer: React.FC<StockContainerProps> = (props) => {
    return (
        <div className="stock-container" style={{opacity: props.opacity}}>
            <span className='stock-label'>{props.legend}</span>
        </div>
    );
};
