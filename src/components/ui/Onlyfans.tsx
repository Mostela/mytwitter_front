import React, { useEffect, useState } from "react";
import { FaTrash, FaVideo } from "react-icons/fa";
import Link from "next/link";
import { IOnlyfans } from "@/interfaces/IOnlyfans";
import { TagModel } from "@/components/ui/TagModel";
import defaultImage from "@/assets/images/loading.gif"
import Image, { StaticImageData } from "next/image";
import { generateLazyLoading } from "@/utils/generateLazyLoading";

interface OnlyfansInterface {
    OFModel: IOnlyfans
    page?: number
    deleteFun: any
    hiddenContent: boolean
}

export const OnlyfansComponent: React.FC<OnlyfansInterface> = (props) => {

    const [page, setPage] = useState<number>(props.page || 0)
    let urlPageContent = props.OFModel.url
    let imageModel: StaticImageData | string = defaultImage

    if(!props.OFModel.model.photo_profile.includes("undefined")){
        imageModel = props.OFModel.model.photo_profile
    }

    if(props.OFModel.url.includes("pornhub")){
        urlPageContent = `/onlyfans/${props.OFModel._id}`
    }

    const pageList = [
        <div className={"onlyfans-card"} key={0} hidden={props.hiddenContent}>
            <Image src={props.OFModel.thumbnail}
                   alt={props.OFModel.name.trim()}
                   className={"img-fluid card-img-top onlyfans-image"}
                   width={500} height={500}
                   loading={"eager"}
                   blurDataURL={generateLazyLoading()}
            />
        </div>,
        <div className={"onlyfans-card"} key={1}>
            <Image src={imageModel}
                   alt={props.OFModel.model.name.trim()}
                   className={"img-fluid card-img-top onlyfans-img-model"}
                   width={500} height={500}
                   loading={"lazy"}
                   placeholder={"blur"}
                   blurDataURL={generateLazyLoading()}
            />
        </div>,
        <div className={"onlyfans-card"} key={2}>
            <div className={"p-3 onlyfans-tags-model"}>
                <div className={"onlyfans-tags-model-items"}>
                    {
                        props.OFModel.model.tags.map((value) => {
                            return <TagModel
                              name={value.name}
                              value={value.value}
                              key={value.key}
                            />
                        })
                    }
                </div>
            </div>
        </div>
    ]

    const deleteItem = (event: React.MouseEvent<SVGElement, MouseEvent>) => {
        event.preventDefault()
        props.deleteFun(props.OFModel._id)
    }

    const nextPage = () => {
        if(props.hiddenContent){
            setPage(1)
            return
        }

        if(page < (pageList.length -1)){
            if(props.OFModel.model.tags.length === 0 && page == 1){
                setPage(0)
            }else{
                setPage(page+1)
            }
        }else {
            setPage(0)
        }
    }

    const convertDuration = (duration: string) => {
        const match = duration.match(/PT(\d+H)?(\d+M)?(\d+S)?/);

        if (match) {
            const hours = match[1] ? parseInt(match[1].slice(0, -1)) : 0;
            const minutes = match[2] ? parseInt(match[2].slice(0, -1)) : 0;
            const seconds = match[3] ? parseInt(match[3].slice(0, -1)) : 0;
            let totalMinutes = hours * 60 + minutes;
            if(totalMinutes > 59){
                totalMinutes = totalMinutes- 60
            }
            return {
                hours: hours > 9 ? hours : `0${hours}`,
                minutes: totalMinutes > 9 ? totalMinutes : `0${totalMinutes}`,
                seconds: seconds > 9 ? seconds : `0${seconds}`,
            };
        }
        return {
            hours: 0,
            minutes: 0,
            seconds: 0,
        };
    }

    const duration = convertDuration(props.OFModel.duration)

    useEffect(() => {
        if(props.hiddenContent){
            setPage(1)
        }else{
            setPage(0)
        }
    }, [props.hiddenContent])

    return (
        <div className={"onlyfans"}>
            {/*Top card*/}
            <div className={"onlyfans-top-card"} onClick={nextPage}>
                {pageList[page]}
            </div>
            {/*Bottom Card*/}
            <div className={"d-flex"}>
                <div className={"flex-column justify-content-center"}>
                    <div className={"text-center p-1 whitespace-nowrap overflow-ellipsis"}>
                        <h4 className={'h4 onlyfans-text'}>{props.OFModel.name}</h4>
                        <span className={"onlyfans-text-modelname"}>{props.OFModel.model.name}</span>
                    </div>
                    <div>
                        <div className={"d-flex flex-row onlyfans-card-aling align-items-center justify-content-around p-4"}>
                            <Link href={urlPageContent} target={"_blank"}>
                                <FaVideo />
                            </Link>
                            <Link href={urlPageContent} target={"_blank"}>
                                <span className={"onlyfans-duration"}>{duration.hours != '00' && `${duration.hours}:`}{duration.minutes}:{duration.seconds}</span>
                            </Link>
                            <span className={"small"}>{new Date(props.OFModel.uploadDate).toLocaleDateString()}</span>
                            <FaTrash onClick={deleteItem} className={"onlyfans-btn-delete"}/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
