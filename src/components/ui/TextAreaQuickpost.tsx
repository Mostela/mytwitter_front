import React from "react";

interface TextAreaQuickpost {
  text: string,
  updateText: React.ChangeEventHandler<HTMLTextAreaElement>
}
export const TextAreaQuickpost: React.FC<TextAreaQuickpost> = (props) => {
  return (
    <textarea className={'form-quickpost-textarea'}
              value={props.text}
              onChange={props.updateText}
    />
  )
}
