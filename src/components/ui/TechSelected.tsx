import { ITechTools } from "@/interfaces/ITechTools";
import React from "react";
import { Pills } from "@/components/ui/Pills";

interface TechSelected {
  techSelected: ITechTools | undefined,
}

export const TechSelected: React.FC<TechSelected> = (props) => {

  return (
    <div className={"techselected d-flex flex-column justify-content-between"}>
      <div>
        <h5 className={"h5 text-capitalize"}>{props.techSelected?.name}</h5>
        <small className={"techselected"}>{props.techSelected?.type}</small>
      </div>
      <div>
        <span className={"techselected"}>Description</span>
        <p className={"techselected"}>{props.techSelected?.description}</p>
      </div>
      <div hidden={!Boolean(props.techSelected?.usedRealLife)}>
        <span className={"techselected"}
              hidden={!Boolean(props.techSelected?.usedRealLife)}
        >This tech had be used in real life, not only in dev tests and discovery tools</span>
      </div>
      <div
        hidden={!props.techSelected?.poc.poc}
        className={"d-flex flex-column justify-content-between"}
      >
        <div>
          <span className={"techselected"} hidden={!props.techSelected?.poc.result}>The POC have good results in your tests</span>
        </div>
        <div>
          <p className={"techselected"}>{props.techSelected?.poc.description}</p>
        </div>
        <div hidden={!props.techSelected?.poc.caseUse}>
          <span className={"techselected"}>Use case tested</span>
          <p className={"techselected"}>{props.techSelected?.poc.caseUse}</p>
        </div>
        <div>
          <div>
            {
              props.techSelected?.tags?.map(value => {
                return <Pills key={value.key}
                              keyValue={value.key}
                              value={value.value}
                              hidden={false}
                              className={"m-1 p-1"}
                              onClick={() => {}}
                />
              })
            }
          </div>
        </div>
      </div>
    </div>
  )
}
