import Link from "next/link";
import React from "react";
import {
    FaAccusoft, FaDev, FaEvernote,
    FaExternalLinkAlt, FaHome,
    FaMoneyBill, FaNewspaper,
    FaPencilAlt,
    FaPowerOff,
    FaUserSecret,
    FaWrench
} from "react-icons/fa";

export enum ICONS_PAGES {
    HOME,
    EXTERNAL_LINK,
    QUICK_POST,
    GIRLS_PHOTO ,
    MONEY ,
    EXIT,
    HOT_LINK,
    TECH,
    NEWS,
    NEWS_TECH,
    NEWS_PERSONAL
}

interface ILinkNavegacao {
    link: string
    icon: ICONS_PAGES
}

export const LinkNavComponent: React.FC<ILinkNavegacao> = (props) => {
    let icon: React.JSX.Element

    switch (props.icon) {
        case ICONS_PAGES.QUICK_POST:
            icon = <FaPencilAlt />
            break
        case ICONS_PAGES.MONEY:
            icon = <FaMoneyBill />
            break
        case ICONS_PAGES.GIRLS_PHOTO:
            icon = <FaAccusoft />
            break
        case ICONS_PAGES.EXIT:
            icon = <FaPowerOff />
            break
        case ICONS_PAGES.EXTERNAL_LINK:
            icon = <FaExternalLinkAlt />
            break
        case ICONS_PAGES.HOT_LINK:
            icon = <FaUserSecret />
            break
        case ICONS_PAGES.TECH:
            icon = <FaWrench />
            break
        case ICONS_PAGES.HOME:
            icon = <FaHome />
            break
        case ICONS_PAGES.NEWS:
            icon = <FaNewspaper />
            break
        case ICONS_PAGES.NEWS_TECH:
            icon = <FaDev />
            break
        case ICONS_PAGES.NEWS_PERSONAL:
            icon = <FaEvernote />
            break
        default:
            icon = <FaHome />
            break
    }

    return(
        <div className={'navbar-custom-btn'}>
            <Link href={`/${props.link}`}>
                {icon}
            </Link>
        </div>
    )
}
