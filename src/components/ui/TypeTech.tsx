import React, { ChangeEventHandler, useState } from "react";
import { TechType } from "@/interfaces/ITechTools";

interface ITypeTech {
  value: TechType
  onChange: ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>
}
export const TypeTech: React.FC<ITypeTech> = (props) => {
  const [typeField, setTypeField] = useState<string>(props.value.toString());

  const changeTypeTech = (event: any) => {
    setTypeField(event.target.value)
    props.onChange(event)
  }

  return (
    <div className={"d-flex flex-column techtype"}>
      <label>Type of tech</label>
      <select onChange={changeTypeTech} value={typeField} className={"techtype-select"}>
        {
          Object.values(TechType).map(value => {
            return <option key={value}
                           value={value}
            className={"techtype-option"}>
              {value.toString()}
            </option>
          })
        }
      </select>
    </div>
  )
}
