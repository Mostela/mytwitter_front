import React from 'react'
import { FaCamera } from 'react-icons/fa'

interface InputFileInterface {
    file: any
}

export const InputFileComponent: React.FC<InputFileInterface> = (props) => {
    const hiddenFileInput = React.useRef<HTMLInputElement>(null);

    const handleClick = (event: React.MouseEvent<HTMLButtonElement>) => {
        hiddenFileInput.current!.click();
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const fileUploaded = event.target.files![0]
        const reader = new FileReader()

        reader.readAsDataURL(fileUploaded)
        reader.onload =  () => {
            props.file({
                base64: reader.result,
                name: fileUploaded.name,
                type: fileUploaded.type,
            })
        }
    }

    return (
        <div>
            <button
                onClick={handleClick}
                type={'button'}
                className={'btn input-file-btn'}
            >
                <FaCamera />
            </button>
            <input
                type="file"
                ref={hiddenFileInput}
                onChange={handleChange}
                style={{ display: 'none' }}
            />
        </div>
    )
}
