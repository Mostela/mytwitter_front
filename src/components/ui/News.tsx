import React from "react";
import Link from "next/link";
import { ToolBarNews } from "@/components/templates/ToolBarNews";

interface News {
  id: string
  title: string
  url: string
  published: string,
  haveContent: boolean
  source: string
  buttonsEnable?: boolean
}

export const News: React.FC<News> = (props) => {
  const notHaveContent = <Link href={props.url} target={"_blank"}><h4 className={"news-text"}>{props.title}</h4></Link>;
  const haveContent = <Link href={`/feed/${props.source.toLowerCase()}/${props.id}`} target={"_parent"}><h4 className={"news-text"}>{props.title}</h4></Link>;

  let classCSS = "news";
  if(props.haveContent){
    classCSS += ' news-have-content';
  }

  return (
    <div className={classCSS}>
      <div className={"d-flex flex-column justify-content-center"}>
        <div>
          <div className={"text-center m-1 p-2 whitespace-nowrap align-content-center align-middle"}>
            {props.haveContent ? haveContent : notHaveContent}
          </div>
        </div>
        <div>
          <ToolBarNews buttonsEnable={props.buttonsEnable} news={props} />
        </div>
      </div>
    </div>
  );
};
