import React, { useState } from "react";

interface Pills {
  keyValue: string
  value: string
  selected?: boolean
  className?: string
  hidden: boolean
  onClick?: any
}
export const Pills: React.FC<Pills> = (props) => {
  const [beClicked, setBeClicked] = useState<boolean>(props.hidden);

  return (
    <button className={`pills ${props.className}`}
            hidden={beClicked}
            onClick={() => {
              setBeClicked(!beClicked)
              return props.onClick({value: props.value, key: props.keyValue})
            }}
            type={"button"}
            value={props.keyValue}>{props.value}
    </button>
  )
}
