import React from "react";
import Image from "next/image";
import loadingImage from "@/assets/images/loading.gif"

interface LoadingInterface {
  show?: boolean;
  loadingText?: string
  children?: React.ReactNode;
}

export const LoadingComponent: React.FC<LoadingInterface> = (props) => {
  const loading_text = props.loadingText || "Aguarde um pouco....";
  return (
    <div className={"loading-div"} hidden={props.show}>
      <h3 className={"loading-text"}>{loading_text}</h3>
      <div className={"loading-box-image"}>
        <Image className={"loading-image img-fluid"} src={loadingImage} alt={"loading circulo girando"} />
      </div>
      {props.children}
    </div>
  );
};
