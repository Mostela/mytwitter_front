import { ImageQuickpost } from "../templates/ImageQuickpost";
import React from "react";
import {Container} from "react-bootstrap";
import {FaCalendarAlt, FaEye} from "react-icons/fa";
import { ButtonDelete } from "@/components/templates/ButtonDelete";

interface QuickpostInterface {
    id: string
    dateCreated: Date
    image: string
    text: string
    views: number
    delete: any
}

export const QuickpostComponent: React.FC<QuickpostInterface> = (props ) => {

    const data = new Date(props.dateCreated)
    const data_created = new Date(data.valueOf() - data.getTimezoneOffset() * 60000);

    return (
      <Container className={'quickpost-box'}>
          <div>
              {
                props.image && <ImageQuickpost image={props.image} text={props.text}/>
              }
          </div>
          <div>
              <h4 className={"h4 quickpost-description"}>{props.text}</h4>
          </div>
          <div className={"d-flex flex-row justify-content-between px-5 quickpost-actions"}>
              <div>
                  <div className={"quickpost-calendar"}>
                            <span className={"quickpost-text"}>
                                <FaCalendarAlt />
                                {` 
                                    ${data_created.getDate()}/
                                    ${data_created.getMonth() + 1} /
                                    ${data_created.getFullYear()}
                                `}
                            </span>
                  </div>
              </div>
              <div>
                  <div className={"quickpost-view"}>
                      <small className={"quickpost-text"}>
                          <FaEye /> {`  `}
                          {props.views}
                      </small>
                  </div>
              </div>
              <div>
                  <ButtonDelete id={props.id} delete={props.delete} />
              </div>
          </div>
      </Container>
    )
}
