import React, { useRef } from "react";
import { INews } from "@/interfaces/INews";
import Link from "next/link";
import {FaHeart, FaShareAlt} from "react-icons/fa";
import { message } from "antd/lib";
import { DeleteNewsPersonal, SaveNews } from "@/state/service/feed";
import { ButtonDelete } from "@/components/templates/ButtonDelete";

interface ToolBarNews {
  news: INews
  buttonsEnable?: boolean
}

export const ToolBarNews: React.FC<ToolBarNews> = (props) => {
  const [messageApi, contextHolder] = message.useMessage();
  const showButtons = useRef(props.buttonsEnable);

  const sharedClick = () => {
    navigator.clipboard.writeText(`${window.location.protocol}//${window.location.host}/feed/${props.news.source.toLowerCase()}/${props.news.id}`);
  }

  const favoriteNews = () => {
    SaveNews(props.news.id).then(value => {
      if(value){
        messageApi.success("News saved")
      }else{
        messageApi.error("News saved before")
      }
    })
  }

  const deleteFavorite = (event: React.MouseEvent<HTMLFormElement>, id: string) => {
    event.preventDefault()
    DeleteNewsPersonal(id).then(() => {
      messageApi.success("News deleted")
    })
  }

  return (
    <div className={"d-flex flex-row justify-content-between w-100 mt-3 px-3 p-1 toolbar-news"}>
      {contextHolder}
      <div>
        <p>{new Date(props.news.published).toLocaleDateString()}</p>
      </div>
      <div className={"text-center"}>
        <Link href={`/feed/${props.news.source.toLowerCase()}`}>
          <p>{props.news.source}</p>
        </Link>
      </div>
      <div className={"align-content-center align-middle"}>
        <button className={"btn btn-outline-light btn-sm"}
                onClick={sharedClick}
                title={"Shared"}><FaShareAlt size={16}/></button>
        <div hidden={!showButtons.current} className={"btn"}>
          <ButtonDelete id={props.news.id}
                        className={"btn btn-sm btn-outline-light"}
                        delete={deleteFavorite}/>
        </div>
        <button hidden={!showButtons.current}
                onClick={favoriteNews}
                className={"btn btn-sm btn-outline-light"}
                title={"Favorite"}><FaHeart size={16}/></button>
      </div>
    </div>
  );
}
