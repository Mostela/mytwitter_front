import React, { useState } from "react";
import {QuickpostComponent} from "../ui/Quickpost";
import { IQuickpost } from "@/interfaces/IQuickpost";
import { DeleteQuickpost } from "@/state/service/DeleteQuickpost";

interface PageQuickpostProps {
    items: Array<IQuickpost>
}

export const PageQuickpost: React.FC<PageQuickpostProps> = (props ) => {
    const [items, setItems] = useState<Array<IQuickpost>>(props.items)

    const deletePost = (event: React.MouseEvent<HTMLFormElement>, id: string) => {
        event.preventDefault()
        DeleteQuickpost(id).then(value => {
            setItems(items.filter(item => {
                return item._id !== id
            }))
        })
    }

    return (
      items.map((post,index) => {
          return <QuickpostComponent
            key={post._id}
            id={post._id}
            text={post.nome}
            dateCreated={post.date_created}
            views={post.visualizacao}
            image={post.image}
            delete={deletePost}
          />
      })
    )
}
