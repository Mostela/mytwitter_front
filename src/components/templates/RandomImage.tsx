import React, { useEffect, useRef, useState } from "react";
import {Image} from "react-bootstrap";
import { GetPhotoUnsplash } from "@/state/service/GetPhotoUnsplash";
import { ITerms, List_terms } from "@/assets/list_terms";
import { LoadingComponent } from "@/components/ui/Loading";

export const RandomImage: React.FC<any> = () => {
  const random_number = Math.floor(Math.random() * List_terms.length) + 1
  const loading = useRef(true);
  const [word] = useState<ITerms>(List_terms[random_number])
  const [dynamicImage, setDynamicImage] = useState<string>()
  const [altDescription,setAltDescription] = useState<string>()

  useEffect(() => {
    if(loading.current){
      GetPhotoUnsplash(word?.dynamicImage).then(r => {
        const {results: [image]} = r;
        const {alt_description, urls} = image;
        setAltDescription(alt_description)
        setDynamicImage(urls.regular);
        loading.current = false
      })
    }
  }, [loading, word?.dynamicImage]);

    return (
        <div className={'word-of-day-wrapper'}>
            <Image fluid src={dynamicImage} alt={altDescription}/>
            <LoadingComponent show={!!dynamicImage} />
            <div className={"word-of-day"}>
              <p className={"word-of-day-text"}>
                { word.word } -
                <strong>
                  { word.author }
                </strong>
              </p>
            </div>
        </div>
    )
}
