import React from "react";
import { FaTrash } from "react-icons/fa";

interface ButtonDelete {
  delete: any,
  id: string,
  className?: string
}

export const ButtonDelete: React.FC<ButtonDelete> = (props) => {
  const className = props.className ? props.className : "quickpost-delete-btn";
  return <div className={className}
                 onClick={x => {
                   props.delete(x,props.id)
                 }}>
    <FaTrash />
  </div>
}
