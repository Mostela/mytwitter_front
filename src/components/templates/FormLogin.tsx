import React, { useContext, useState } from "react";
import { TextFieldComponent } from "@/components/ui/TextField";
import { LoginService } from "@/state/service/Login";
import { UserLoggedContext } from "@/state/context/UserLogged";
import { SetLocalStorage } from "@/state/provider/localstorage";
import { message } from "antd/lib";

interface FormLoginInterface {}

export const FormLoginComponent: React.FC<FormLoginInterface> = () => {
  const userLogged = useContext(UserLoggedContext)
  const [email, setEmail] = useState<string>("");
  const [password, setPassword] = useState<string>("");
  const [messageApi, contextHolder] = message.useMessage();

  const checkLogin = () => {
    if (email && password) {
      LoginService(email, password).then(value => {
        const {data: {status, session}, } = value;
        if (status) {
          userLogged.setLogged(true)
          SetLocalStorage("logged", true);
          SetLocalStorage("session", session);
        } else {
          messageApi.error("Senha ou email incorretos");
          userLogged.setLogged(false)
        }
      }).catch(() => {
        messageApi.error("Senha ou email incorretos");
        userLogged.setLogged(false)
      })
    }
  };

  const onChangeEmail = (x: React.ChangeEvent<HTMLInputElement>) => {
    setEmail(x.target.value);
  };

  const onChangePassword = (x: React.ChangeEvent<HTMLInputElement>) => {
    setPassword(x.target.value);
  };

  return (
    <form autoComplete={"false"} className={"loginbox"} onSubmit={checkLogin}>
      {contextHolder}
      <h2 className={"text-center"}><b>My Twitter</b></h2>
      <div className={"d-flex justify-content-center"}>
        <TextFieldComponent label="Email" onChange={onChangeEmail} value={email} />
      </div>
      <div className={"d-flex justify-content-center"}>
        <TextFieldComponent label={"Senha"}
                            type={"password"}
                            value={password}
                            onChange={onChangePassword}
        />
      </div>
      <div className={"btn-login-div mt-4 mb-2"}>
        <button className={"btn btn-login"}
                type={"button"}
                onClick={checkLogin}>
          Sing in
        </button>
      </div>
    </form>
  );
};
