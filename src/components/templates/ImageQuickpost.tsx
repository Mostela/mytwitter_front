import React from "react";
import {Image} from "react-bootstrap";

interface ImageQuickpost{
    image: string,
    text: string,
}

export const ImageQuickpost: React.FC<ImageQuickpost> = (props) => {
    return (
        <div className={"image-quickpost"}>
          <Image fluid className={"image-quickpost-img"} src={props.image} alt={props.text}/>
        </div>
    )
}
