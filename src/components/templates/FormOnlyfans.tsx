import {Form} from "react-bootstrap";
import React, {useState} from "react";
import {BtnPostComponent} from "../ui/BtnPost";
import { TextFieldComponent } from "@/components/ui/TextField";

interface FormOnlyfansInterface {
    setData: any
}

export const FormOnlyfansComponent: React.FC<FormOnlyfansInterface> = (props) => {
    const [modelName, setModelName] = useState<string>("")
    const [link, setLink] = useState<string>("")

    const cleanData = () => {
        setModelName("")
        setLink("")
    }

    const send = (event: React.MouseEvent<FormDataEvent>) => {
        event.preventDefault()
        props.setData(
            {
                model: {
                    name: modelName
                },
                url: link,
            }
        )
        cleanData()
    }

    return(
        <Form className={"form-onlyfans"}>
            <div className={'d-flex justify-content-center'}>
                <h3 className={"form-onlyfans-title"}>Onlyfans</h3>
            </div>
            <div>
                <div className={'d-flex justify-content-center'}>
                    <TextFieldComponent label="Model name" type={"text"}
                                        value={modelName}
                                        onChange={x => {
                                            setModelName(x.target.value)
                                        }}/>
                </div>
                <div className={'d-flex justify-content-center'}>
                    <TextFieldComponent label="Link" type={"text"}
                                        value={link}
                                        onChange={x => {
                                            setLink(x.target.value)
                                        }}
                    />
                </div>
                <div className={'d-flex justify-content-center'}>
                    <BtnPostComponent sending={send} status={false} />
                </div>
            </div>
        </Form>
    )
}
