import React, {useState} from "react";
import { ImageQuickpost } from "./ImageQuickpost";
import {Col, Form, Row, Container} from "react-bootstrap";
import {InputFileComponent} from "../ui/InputFile";
import {BtnPostComponent} from "../ui/BtnPost";
import { NewQuickpostService } from "@/state/service/NewQuickpost";
import { PhotoUploadFile, PhotoUploadSignature } from "@/state/service/PhotoUpload";
import { TextAreaQuickpost } from "@/components/ui/TextAreaQuickpost";
import { message } from "antd/lib";

interface IFiles {
    base64: any,
    name: string,
    type: string
}

interface IFormQuickpost {
    onSubmit: any
}

export const FormQuickpost: React.FC<IFormQuickpost> = (props) => {
    const [btnStatus, setBtnStatus] = useState(false)
    const [files, setFiles] = useState<IFiles>({
        base64: "",
        name: "",
        type: ""
    })
    const [text, setText] = useState<string>("")
    const [messageApi, contextHolder] = message.useMessage();

    const setImageToUpload =(value: IFiles) => {
        setFiles(value)
    }

    const updateText = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
        setText(event.target.value)
        if(text.length >= 2){
            setBtnStatus(true)
        }else{
            setBtnStatus(false)
        }
    }

    const sendPost = async (e: React.MouseEvent<HTMLFormElement>) => {
        e.preventDefault()
        messageApi.open({
            content: "New post created",
            type: "loading"
        })
        let imageURL: string = "";
        let fieldsUpload: any
        if (files.base64) {
            fieldsUpload = await PhotoUploadSignature("quickpost")
            const {fields: {key}} = fieldsUpload
            imageURL = key
        }
        NewQuickpostService({
            "image": imageURL,
            "nome": text
        }).then(async responseCreate => {
            if (imageURL && responseCreate.status) {
                const { url, fields } = fieldsUpload;
                messageApi.open({
                    content: "New post created",
                    type: "success"
                })
                await PhotoUploadFile({
                    url,
                    fields: {
                        'x-amz-security-token': fields['x-amz-security-token'],
                        'AWSAccessKeyId': fields['AWSAccessKeyId'],
                        'policy': fields['policy'],
                        'signature': fields['signature'],
                        'key': fields['key']
                    },
                    file: Buffer.from(files.base64.replace(`data:${files.type};base64,`, ""), "base64")
                }).then(resp => {
                    messageApi.open({
                        content: "Image uploaded",
                        type: "success"
                    })
                    return resp
                })
            }
            setText("")
            setBtnStatus(false)
            props.onSubmit(responseCreate.data)
        })
    }


    return(
        <Form>
            {contextHolder}
            <Container className={'form-quickpost-box'}>
                <Row>
                    <Col>
                        <h4 className={'text-center'}>Nova postagem rapida</h4>
                        <p className={"text-left"}>Isso é lido apenas por você</p>
                        {
                            !text ? <small>No que esta pensando...</small> : <></>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col>
                        <TextAreaQuickpost text={text} updateText={updateText} />
                    </Col>
                </Row>
                <Row>
                    <Col>
                        {
                            files.base64 && <ImageQuickpost image={files.base64} text={""}/>
                        }
                    </Col>
                </Row>
                <Row>
                    <Col className={
                        'btn-group'
                    }>
                        <InputFileComponent file={setImageToUpload} />
                        <BtnPostComponent status={!btnStatus} sending={sendPost}/>
                    </Col>
                </Row>
            </Container>
        </Form>
    )
}
