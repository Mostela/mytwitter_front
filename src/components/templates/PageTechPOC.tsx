import React, { useContext } from "react";
import { ITechTools } from "@/interfaces/ITechTools";
import { TechItemList } from "@/components/ui/TechItemList";
import Link from "next/link";
import { TechSelectContext } from "@/state/context/TechSelect";

interface IPageTechPOC {
  data: Array<ITechTools>,
  children?: React.ReactNode
}
export const PageTechPOC: React.FC<IPageTechPOC> = (props) => {
  const contextSelect = useContext(TechSelectContext);

  const changeContext = (value: ITechTools) => {
    contextSelect.setTechSelect(value)
  }

  return (
    <div className={"d-flex flex-column justify-content-center page-tech-poc"}>
      <div className={"text-center"}>
        <h3 className={"h3"}>Proves Of Concept</h3>
      </div>
      <div className={"justify-content-center"}>
        {
          props.data?.slice(0, 10).filter(x => Boolean(x.poc.poc)).map(value => {
            return <div
              key={value._id}
              onClick={() => changeContext(value)}
            >
              <TechItemList
                name={value.name}
                key={value._id}
                type={value.type.toString()}
              />
            </div>
          })
        }
      </div>
      {
        props.children
      }
    </div>
  )
}
