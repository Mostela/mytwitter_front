import React, { ChangeEvent, useEffect, useRef, useState } from "react";
import Hls from 'hls.js';
import { Button, Slider } from "@material-ui/core";
import { Mark } from "@material-ui/core/Slider/Slider";
interface VideoPlayerInterface {
  sourceURL: string
  videoTags?: string
}
export const VideoPlayer: React.FC<VideoPlayerInterface> = (props) => {
  const videoRef = useRef<any>();
  const [playingVideo, setPlayingVideo] = useState(true);
  const [currentTimeVideo, setCurrentTimeVideo] = useState(0);
  const [videoDuration, setVideoDuration] = useState(0);

  const marksVideo: Array<Mark>|boolean = props.videoTags?.split(",").map(value => {
    const data = value.split(":");
    const valueMark: Mark = {
      value: Number(data[1]),
      label: <span className={"video-player-control-button"}>{data[0]}</span>,
    }
    return valueMark
  })|| false

  useEffect(() => {
    const video = videoRef.current;

    if (Hls.isSupported() && video) {
      const hls = new Hls();
      hls.loadSource(props.sourceURL);
      hls.attachMedia(video);
      hls.on(Hls.Events.MEDIA_ATTACHED, () => {
        video.muted = true;
        video.play();
      });
    }
  }, [props.sourceURL]);

  const handlerPause = () => {
    const video = videoRef.current;
    video.pause();
    setPlayingVideo(false)
  }
  const handlerPlay = () => {
    const video = videoRef.current;
    video.play();
    setPlayingVideo(true)
  }

  const handlerMute = () => {
    const video = videoRef.current;
    video.muted = !video.muted;
  }

  const handleProgress = () => {
    if(!videoDuration){
      setVideoDuration(videoRef.current.duration);
    }
    setCurrentTimeVideo(videoRef.current.currentTime)
  }

  const handleMovieSkip = () => {
    videoRef.current.currentTime = currentTimeVideo;
    handlerPlay()
  }

  const onChangeTimeVideo =  (event: ChangeEvent<{}>, value: number | number[]) => {
    handlerPause();
    setCurrentTimeVideo(value as number);
  };

  const handleFullScreen = () => {
    videoRef.current.requestFullscreen();
  }


  return (
    <div className="d-flex flex-column gap-2">
      <video className="w-100"
             ref={videoRef}
             onPause={handlerPause}
             onPlay={handlerPlay}
             onTimeUpdate={handleProgress}
             onClick={handlerPause}
             controls={false} />
      {/* Progress bar */}
      <div className="w-100 flex-row d-flex">
        <Slider value={currentTimeVideo} min={0} max={videoDuration}
                color={"secondary"}
                marks={marksVideo}
                onChangeCommitted={handleMovieSkip}
                onChange={onChangeTimeVideo} />
      </div>
      <div className="video-player-control">
        <div className="d-flex gap-2 align-items-center justify-content-center">
          <Button hidden={playingVideo} onClick={handlerPlay}>
            <PlayIcon />
          </Button>
          <Button hidden={!playingVideo} onClick={handlerPause}>
            <PauseIcon />
          </Button>
          <Button onClick={handlerMute}>
            <VideoIcon />
          </Button>
          <Button onClick={handleFullScreen}>
            <MaximizeIcon />
          </Button>
        </div>
      </div>
    </div>
  )
}

function MaximizeIcon() {
  return (
    <svg
      className={"w-4 h-4 video-player-control-button"}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="M8 3H5a2 2 0 0 0-2 2v3" />
      <path d="M21 8V5a2 2 0 0 0-2-2h-3" />
      <path d="M3 16v3a2 2 0 0 0 2 2h3" />
      <path d="M16 21h3a2 2 0 0 0 2-2v-3" />
    </svg>
  );
}


function PauseIcon() {
  return (
    <svg
      className={"w-4 h-4 video-player-control-button"}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <rect width="4" height="16" x="6" y="4" />
      <rect width="4" height="16" x="14" y="4" />
    </svg>
  )
}


function PlayIcon() {
  return (
    <svg
      className={"w-4 h-4 video-player-control-button"}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <polygon points="5 3 19 12 5 21 5 3" />
    </svg>
  )
}


function VideoIcon() {
  return (
    <svg
      className={"w-4 h-4 video-player-control-button"}
      xmlns="http://www.w3.org/2000/svg"
      width="24"
      height="24"
      viewBox="0 0 24 24"
      fill="none"
      stroke="currentColor"
      strokeWidth="2"
      strokeLinecap="round"
      strokeLinejoin="round"
    >
      <path d="m22 8-6 4 6 4V8Z" />
      <rect width="14" height="12" x="2" y="6" rx="2" ry="2" />
    </svg>
  )
}
