import { ITechTools } from "@/interfaces/ITechTools";
import React from "react";
import { message, TabsProps } from "antd/lib";
import { Tabs } from 'antd/lib';
import { TechSelected } from "@/components/ui/TechSelected";
import { FormNewTech } from "@/components/templates/FormNewTech";
import { EditTechTools } from "@/state/service/techlist";

interface TechTabs {
  techSelected: ITechTools | undefined,
}

export const TechTabs: React.FC<TechTabs> = (props) => {
  const [messageApi, contextHolder] = message.useMessage();

  const createNewTech = (newItem: ITechTools) => {
    const idTech = props.techSelected?._id
    if(idTech){
      EditTechTools(newItem, idTech).then(value => {
        if(value.status){
          messageApi.open({
            type: "loading",
            content: `Send new tech ${newItem.name} to create`
          })
        }
      })
    }
  }

  const items: TabsProps['items'] = [
    {
      key: '1',
      label: 'Tech infos',
      children: <TechSelected techSelected={props.techSelected} />,
    },
    {
      key: '2',
      label: 'Edit',
      children: <FormNewTech onSubmit={createNewTech}
                             newTech={props.techSelected}
                             techID={props.techSelected?._id}
      />,
    },
  ];

  return (
    <>
      <Tabs defaultActiveKey="1" items={items} />
      {contextHolder}
    </>
  );
}
