import React, { useContext } from "react";
import { FaPowerOff } from "react-icons/fa";
import { useRouter } from "next/router";
import { UserLoggedContext } from "@/state/context/UserLogged";
import { SetLocalStorage } from "@/state/provider/localstorage";
import { message } from "antd/lib";

interface ButtonLogoff {

}

export const ButtonLogoff: React.FC<ButtonLogoff> = () => {
  const router = useRouter()
  const userLogged = useContext(UserLoggedContext)
  const [messageApi, contextHolder] = message.useMessage();

  const logoff = (event: React.MouseEvent<HTMLDivElement>) => {
    event.preventDefault();
    SetLocalStorage("logged", false)
    SetLocalStorage("session", {});
    userLogged.setLogged(false);
    messageApi.open({
      content: "User logout",
      type: "success"
    })
    router.push("/").then();
  }

  return (
    <div onClick={logoff} className={'navbar-custom-btn'}>
      {contextHolder}
      <FaPowerOff />
    </div>
  )
}
