import React, { useRef, useState } from "react";
import { TextFieldComponent } from "@/components/ui/TextField";
import { ITechTags, ITechTools, TechType } from "@/interfaces/ITechTools";
import { Switch } from "@material-ui/core";
import { TextAreaQuickpost } from "@/components/ui/TextAreaQuickpost";
import { TypeTech } from "@/components/ui/TypeTech";
import { BtnPostComponent } from "@/components/ui/BtnPost";
import { GetTechTags } from "@/state/service/GetTechTags";
import { Pills } from "@/components/ui/Pills";

interface INewTech {
  newTech?: ITechTools,
  techID?: string
  onSubmit: any
}

export const FormNewTech: React.FC<INewTech> = (props) => {
  const [nameField, setNameField] = useState<string>(props.newTech?.name || "")
  const [urlField, setURLField] = useState<string>(props.newTech?.url || "")
  const [usedRealLife, setUsedRealLife] = useState<boolean>(props.newTech?.usedRealLife || false)
  const [typeTech, setTypeTech] = useState<TechType>(props.newTech?.type || TechType.FRAMEWORK)
  const [description, setDescription] = useState<string>(props.newTech?.description || "");
  const [pocHave, setPocHave] = useState<boolean>(props.newTech?.poc.poc || false);
  const [pocResult, setPocResult] = useState<boolean>(props.newTech?.poc.result || false)
  const [pocCaseUse, setPocCaseUse] = useState<string>(props.newTech?.poc.caseUse || "")
  const [pocDescription, setPocDescription] = useState<string>(props.newTech?.poc.description || "")
  const [tagsHidden, setTagsHidden] = useState<boolean>(false)

  const [techTagsSelected, setTechTagsSelected] = useState<Array<ITechTags>>([])

  const [techTags, setTechTags] = useState<Array<ITechTags>>()
  const loadingTags = useRef<boolean>(true)

  const updateDescription = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setDescription(event.target.value);
  }
  const changeTypeTech = (event: React.ChangeEvent<HTMLTextAreaElement>) => {
    setTypeTech(TechType[event.target.value as keyof typeof TechType]);
  }

  const selectedTechTag = (value: ITechTags) => {
    techTagsSelected.push(value)
    setTechTagsSelected(techTagsSelected)
  }

  const resetList = () => {
    setDescription("")
    setURLField("")
    setNameField("")
    setUsedRealLife(false)
    setTechTagsSelected([])
    setPocHave(false)
    setPocResult(false)
    setPocCaseUse("")
    setPocDescription("")
    setTagsHidden(false)
  }

  if(loadingTags.current){
    loadingTags.current = !loadingTags.current
    GetTechTags().then(value => {
      const {data} = value;
      if(data){
        setTechTags(data);
      }
    })
  }

  const sendingNewTech = (event: React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault()
    const techToolNew: ITechTools = {
      description,
      type: typeTech,
      usedRealLife,
      name: nameField,
      url: urlField,
      _id: "",
      date_created: new Date(),
      tags: techTagsSelected || [],
      poc: {
        poc: pocHave,
        _id: "",
        description: pocDescription,
        caseUse: pocCaseUse,
        dataDone: new Date(),
        result: pocResult
      }
    }
    props.onSubmit(techToolNew)
    resetList()
  }

  return (
    <form className={"form-control flex-column justify-content-around"}>
      <div className={"m-1"}>
        <div className={"text-center"}>
          <span className={"text-center"}>Infos tech</span>
        </div>
        <div className={"d-flex flex-column"}>
          <div className={"d-flex justify-content-center my-3"}>
            <TextFieldComponent label={"Name"}
                                value={nameField}
                                onChange={x => setNameField(x.target.value)}
                                type={"text"} />
          </div>
          <div className={"d-flex justify-content-center my-3"}>
            <TextFieldComponent label={"URL for main page"}
                                value={urlField}
                                onChange={x => setURLField(x.target.value)}
                                type={"url"} />
          </div>
          <div className={"d-flex justify-content-between my-3"}>
            <div className={"text-center"}>
              <label>Used on real life before</label>
              <Switch className={"btn btn-success"}
                      value={usedRealLife}
                      onChange={() => setUsedRealLife(!usedRealLife)}
                      onClick={() => setUsedRealLife(!usedRealLife)}
              />
            </div>
            <div>
              <TypeTech value={typeTech} onChange={changeTypeTech} />
            </div>
          </div>
          <div className={"d-flex flex-column justify-content-center my-3"}>
            <label className={"text-center"}>Give a shot description about this tool</label>
            <TextAreaQuickpost text={description} updateText={updateDescription} />
          </div>
          <div className={"d-flex flex-column my-3"}>
            <div className={"d-flex flex-column align-items-center"}>
              <label>Tell more about the Prove Of Concept - POC</label>
              <Switch className={"btn btn-success"}
                      value={pocHave}
                      onChange={() => setPocHave(!pocHave)}
                      onClick={() => setPocHave(!pocHave)}
              />
            </div>
            <div hidden={!pocHave}>
              <div className={"d-flex flex-column align-items-center"}>
                <div className={"d-flex flex-column my-1 w-100"}>
                  <label className={"text-center"}>Write about the case use</label>
                  <TextAreaQuickpost text={pocCaseUse}
                                     updateText={x => setPocCaseUse(x.target.value)} />
                </div>
                <div className={"d-flex flex-column w-100 my-1"}>
                  <label className={"text-center"}>write about the results</label>
                  <TextAreaQuickpost text={pocDescription}
                                     updateText={x => setPocDescription(x.target.value)} />
                </div>
                <div className={"d-flex flex-row justify-content-around"}>
                  <div className={"d-flex flex-column align-items-center"}>
                    {/*  CREATE NEW COMPONENT FOR CALENDAR*/}
                  </div>
                  <div className={"d-flex flex-column align-items-center"}>
                    <label>POC have good results?</label>
                    <Switch className={"btn btn-success"}
                            value={pocResult}
                            onChange={() => setPocResult(!pocResult)}
                            onClick={() => setPocResult(!pocResult)} />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className={"d-flex justify-content-between my-3"}>
            <div>
              {
                techTags?.map(value => {
                  return <Pills key={value.key}
                                keyValue={value.key}
                                value={value.value}
                                hidden={tagsHidden}
                                className={"m-1 p-1"}
                                onClick={selectedTechTag}
                  />
                })
              }
            </div>
          </div>
          <div className={"d-flex flex-row justify-content-around my-3"}>
            <button className={"btn btn-post"}
                    type={"reset"}
                    onClick={resetList}
            >Reset list</button>
            <BtnPostComponent status={!(!!nameField && !!urlField)}
                              text={!props.techID ? "Save!" : "Edit"}
                              sending={sendingNewTech}/>
          </div>
        </div>
      </div>
    </form>
  )
}
