import React from 'react';
import {StockBarContainers} from "@/components/ui/StockBarContainer";

const StockBarChart = ({ data }: { data: Array<IStockChart> }) => {
    return (
        <div className="stock-chart-container">
            {data.map((stock, index) => (
                <StockBarContainers
                    key={index}
                    title={stock.label}
                    containers={stock.containers}
                />
            ))}
        </div>
    );
};

export default StockBarChart;
