import React, {useEffect, useRef, useState} from "react";

interface StockInfoProps {
    fluctuation: string
    lastPrice: number
}

interface StockCardInfoProps {
    legend: string
    stockInfo: Array<StockInfoProps>
    onClick: React.MouseEventHandler<HTMLDivElement>
    selected?: boolean
}

export const StockInfo: React.FC<StockInfoProps> = (props) => {
    return (
        <div className="align-content-start p-1">
            <h2 className={"h2"}>{props.fluctuation}</h2>
            <span className={""}>R$ {props.lastPrice.toPrecision(4)}</span>
        </div>
    );
};

export const clearTicketName = (name: string): string => {
    return name.split(".")[0]
}

export const StockCard: React.FC<StockCardInfoProps> = (props) => {
    const [selected, setSelected] = useState<boolean>(props.selected ?? false)
    const classNameOriginal = "d-flex flex-column align-content-around "
    const selectedClassName = "stock-card-info-select"
    const unselectedClassName = "stock-card-info"
    const className = useRef(classNameOriginal + unselectedClassName)

    useEffect(() => {
        if (selected) {
            className.current = classNameOriginal + selectedClassName
        } else {
            className.current = classNameOriginal + unselectedClassName
        }
    }, [className, selected]);

    const handlerClick = (e: React.MouseEvent<HTMLDivElement>) => {
        setSelected(!selected)
        props.onClick(e)
    }

    return (
        <div className={className.current} onClick={handlerClick}>
            <h1 className={"h1 text-uppercase fw-bold"}>{props.legend}</h1>
            <div className={"d-flex flex-column align-content-around"}>
                {props.stockInfo.map((elem, i) => (
                    <StockInfo key={i} fluctuation={elem.fluctuation} lastPrice={elem.lastPrice}/>
                ))}
            </div>
        </div>
    );
};
