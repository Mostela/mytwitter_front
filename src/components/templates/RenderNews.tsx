import { INews } from "@/interfaces/INews";
import {Col, Row} from "react-bootstrap";
import { News } from "@/components/ui/News";
import React from "react";

interface IRenderNews {
  data: Array<INews>
  key: number
}
export const RenderItems: React.FC<IRenderNews> = (props) => {

  const list_news = props.data.map((value, index) => {
      return <Col md={2} className={"p-3"} key={index}>
          <News id={value.id}
            key={value.id}
            title={value.title}
            url={value.url}
            published={value.published}
            haveContent={value.haveContent}
            source={value.source}
            buttonsEnable={true}
      />
      </Col>
  })

  return <Row className={"pb-3"} key={props.key}>{list_news}</Row>;
}
