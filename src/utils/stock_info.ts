const regex = /\b[A-Za-z]{4}3\b/g;

export const stockInfoLink = (text: string) => {
  return text.replace(regex, (match) => `<a href='/stock/${match}' class='link-stock'>${match}</a>`);
}
