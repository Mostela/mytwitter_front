import { api } from "@/state/provider/axios";

interface NewOnlyfansInterface {
  model: string
  link: string
}

export const NewOnlyfansService = (data: NewOnlyfansInterface) => {
  return api.post("onlyfans", data).then(r => {
    return {status: r.status === 200, data: r.data}
  })
}
