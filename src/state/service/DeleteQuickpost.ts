import { api } from "@/state/provider/axios";

export const DeleteQuickpost = (id: string) => {
  return api.delete(`quickpost/${id}`).then(value => {
    return value.status === 202
  })
}
