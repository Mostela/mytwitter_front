import { api } from "@/state/provider/axios";

export const DeleteOnlyfans = (guid: string) => {
  return api.delete(`onlyfans/${guid}`).then(value => {
    return !!value.status
  })
}
