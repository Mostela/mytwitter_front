import { api } from "@/state/provider/axios";
import axios from "axios";

export const PhotoUploadSignature = (resource: string) => {
  return api.post("/photo", {resource}).then(value => {
    return value.data;
  })
}

export const PhotoUploadFile = (data: {
  url: string,
  file: any,
  fields: any
}) => {
  const url = new URL(data.url);

  return axios.postForm(url.href, {
    ...data.fields,
    file: data.file
  }, {
    headers: {
      'x-amz-storage-class': 'STANDARD_IA'
    }
  }).then(value => {
    return value.status === 204
  })
}
