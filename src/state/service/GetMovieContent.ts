import { APIBackend } from "@/state/provider/axios";

export const callMovieEndpoint = async (id: string) => {
  return new APIBackend().post({
    endpoint: `onlyfans/movie/${id}`,
  }).then(value => {
    return value.data
  })
}
