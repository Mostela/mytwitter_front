import { api } from "@/state/provider/axios";

export const LoginService = async (email: string, password: string) => {
  const value = await api.post(`login`, {
    email,
    password: btoa(password.toString())
  });
  return {data: value.data, status: value.status};
}
