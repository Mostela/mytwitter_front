import {APIBackend} from "@/state/provider/axios";

export const GetStockListOperation = () => {
    return new APIBackend().get({
        endpoint: `stocks/operation`,
    }).then(value => {
        const {data} = value.data
        return data
    }).catch(() => {
        return []
    })
}

export const GetStockOperation = (ticket_id: string, period?: string) => {
    return new APIBackend().get({
        endpoint: `stocks/operation`,
        searchParams: {
            stock: ticket_id,
            period: period || '5d'
        }
    }).then(value => {
        const {data} = value.data
        return data
    }).catch(() => {
        return []
    })
}

export const GetJackpot = () => {
    return new APIBackend().get({
        endpoint: `stocks/jackpot`,
    }).then(value => {
        return value.data
    }).catch(() => {
        return {}
    })
}
