import { APIBackend } from "@/state/provider/axios";

export const ReturnQuickpost = (start: number) => {
  return new APIBackend().get({
    endpoint: `quickpost?start=${start}`
  }).then(value => {
    return value.data;
  })
}

export const ReturnQuickpostServer = () => {
  return new APIBackend().get({
    endpoint: "quickpost"
  }).then(value => {
    return value.data;
  })
}
