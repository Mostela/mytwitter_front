import { apiUnsplash } from "@/state/provider/unsplash";


export const GetPhotoUnsplash = (queryTerm: string) => {
  return apiUnsplash.get(`search/photos?query=${queryTerm}`)
    .then(value => {
      const {data} = value;
      return data
    }).catch(() => {
      return {}
    })
}
