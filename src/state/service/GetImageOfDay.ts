import { APIBackend } from "@/state/provider/axios";

export const GetImageOfDay
  = () => {
  const api = new APIBackend();
  return api.get({
    endpoint: "photo/day"
  }).then(value => {
    return value.data;
  })
}
