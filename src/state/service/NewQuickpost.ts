import { api } from "@/state/provider/axios";

interface NewQuickpostInterface {
    nome: string
    image: string
}

export const NewQuickpostService = (data: NewQuickpostInterface) => {
    return api.post("quickpost", data).then(r => {
        return {status: r.status === 201, data: r.data}
    })
}
