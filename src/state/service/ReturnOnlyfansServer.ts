import { APIBackend } from "@/state/provider/axios";

export const ReturnOnlyfansServer = (cacheKey: string) => {
  return new APIBackend().get({
    endpoint: "onlyfans",
    headers: {
      'mytwitter-cache': cacheKey
    }
  }).then(value => {
    return {
      ...value.data,
      headers: value.headers,
    };
  })
}
