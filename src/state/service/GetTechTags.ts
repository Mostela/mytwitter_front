import { api } from "@/state/provider/axios";

export const GetTechTags = () => {
  return api.get(`techlist/tags`).then(value => {
    return value.data
  })
}
