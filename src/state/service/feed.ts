import { APIBackend } from "@/state/provider/axios";

export const GetCNNNews = () => {
  return new APIBackend().get({
    endpoint: 'feed/news',
  }).then(value => {
    return value.data
  })
}

export const GetCNNNewsSearch = (id: string) => {
  return new APIBackend().get({
    endpoint: 'feed/news',
    searchParams: { 'id': id }
  }).then(value => {
    return value.data
  })
}

export const GetNewsLatest =(page?: number) => {
  return new APIBackend().get({
    endpoint: 'feed/latest',
    searchParams: {
      page: page || 0
    }
  }).then(value => {
    return value.data
  })
}

export const GetNewsLatestSource = async (source: string, page?: number) => {
  const value = await new APIBackend().get({
    endpoint: 'feed/latest',
    searchParams: {
      "source": source,
      page: page || 0
    }
  });
  return value.data;
}

export const GetNewsPersonal = (page?: number) => {
  return new APIBackend().get({
    endpoint: 'feed/personal',
    searchParams: {
      page: page || 0
    }
  }).then(value => {
    return value.data
  })
}

export const GetNewsLatestID = (id: string) => {
  return new APIBackend().get({
    endpoint: `feed/${id}`,
  }).then(value => {
    return value.data
  })
}

export const SaveNews = (id: string) => {
  return new APIBackend().post({
    endpoint: `feed/personal/${id}`,
  }).then(value => {
    return value.status == 202
  }).catch(() => {
    return false
  })
}

export const DeleteNewsPersonal = (id: string) => {
  return new APIBackend().delete({
    endpoint: `feed/personal/${id}`,
  }).then(value => {
    return value.status == 202
  }).catch(() => {
    return false
  })
}
