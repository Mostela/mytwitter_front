import { APIBackend } from "@/state/provider/axios";
import { ITechTools } from "@/interfaces/ITechTools";
import { ITechToolsList } from "@/interfaces/ITechTools";

export const NewTechTags = (data: ITechTools) => {
  return new APIBackend().post({
    endpoint: 'techlist',
    body: JSON.stringify(data),
  }).then(value => {
    return {
      status: value.status === 201,
      data: value.data
    }
  })
}

export const DeleteTechTools = (id: string) => {
  return new APIBackend().delete({
    endpoint: `techlist/${id}`,
  }).then(value => {
    return {
      status: value.status === 201,
      data: value.data
    }
  })
}

export const EditTechTools = (data: ITechTools, id: string) => {
  return new APIBackend().patch({
    endpoint: `techlist/${id}`,
    body: JSON.stringify(data),
  }).then(value => {
    return {
      status: value.status === 202,
      data: value.data
    }
  })
}

interface getTechParams {
  searchParams?: object
}

export const GetTechTools = (req: getTechParams): Promise<ITechToolsList> => {
  return new APIBackend().get({
    endpoint: 'techlist',
    searchParams: req.searchParams,
  }).then(value => {
    return value.data;
  })
}
