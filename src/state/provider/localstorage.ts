const isBrowser = () => typeof window !== "undefined"

export const SetLocalStorage = (key: string, value: any) => {
  if (isBrowser()) {
    localStorage.setItem(key, value)
  }
}

export const GetLocalStorage = (key: string) => {
  if (isBrowser()){
    return localStorage.getItem(key)
  }
}
