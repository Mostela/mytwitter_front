import axios from "axios"

export const api = axios.create({
  baseURL: process.env.BACKEND_HOST
})

enum RequestMethod {
  POST= "POST",
  GET = "GET",
  PUT = "PUT",
  PATCH = "PATCH",
  DELETE = "DELETE"
}

interface RequestInterface {
  endpoint: string
  searchParams?: object
  headers?: object
  body?: any
}

interface ReturnRequest {
  data: any,
  status: number
  headers: Headers
}

class APIBackend {
  protected callEndpoint = async (req: RequestInterface, method: RequestMethod): Promise<ReturnRequest>=> {
    const urlToCall = new URL(`${process.env.BACKEND_HOST}/${req.endpoint}`)

    if(req.searchParams){
      Object.keys(req.searchParams).forEach(value => {
        // @ts-ignore
        urlToCall.searchParams.set(value, req.searchParams[value])
      })
    }

    const response = await fetch(urlToCall.href, {
      body: req.body,
      headers: {
        ...req.headers,
      },
      method
    })

    return {data: await response.json(), status: response.status,
      headers: response.headers}
  }

  get = async (req: RequestInterface) => {
    return this.callEndpoint({
      ...req,
    }, RequestMethod.GET)
  }

  post = async (req: RequestInterface) => {
    return this.callEndpoint({
      ...req,
    }, RequestMethod.POST)
  }

  patch = async (req: RequestInterface) => {
    return this.callEndpoint({
      ...req,
    }, RequestMethod.PATCH)
  }

  put = async (req: RequestInterface) => {
    return this.callEndpoint({
      ...req,
    }, RequestMethod.PUT)
  }

  delete = async (req: RequestInterface) => {
    return this.callEndpoint({
      ...req,
    }, RequestMethod.DELETE)
  }
}

export {
  APIBackend
}
